import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.Random;

public class NetAddrWithTime extends Payload {
    //Variables used to unpack
    private int timeInt;
    private long servicesLong;
    private String addressString;
    private short portShort;

    //Variables used to pack
    private byte[] time;
    private byte[] services;
    private byte[] address;
    private byte[] port;

    //Getters
    public int getTimeInt() { return this.timeInt; }
    public long getServicesLong() { return this.servicesLong; }
    public String getAddressString() { return this.addressString; }
    public short getPortShort() { return this.portShort; }

    //Generate Test Case
    public static NetAddrWithTime GenerateTest() {
        Random random = new Random();
        String randAddress = (random.nextInt(256)) + "." + (random.nextInt(256)) + "." + (random.nextInt(256)) + "." + (random.nextInt(256));

        return NetAddrWithTime.Generate(
                (int) Generator.generateTimestamp(),
                random.nextLong(),
                randAddress,
                (short)random.nextInt(16384)
        );
    }

    //Generate
    public static NetAddrWithTime Generate(int time, long services, String address, short port) {
        NetAddrWithTime netAddrWithTime = new NetAddrWithTime();
        netAddrWithTime.timeInt = time;
        netAddrWithTime.servicesLong = services;
        netAddrWithTime.addressString = address;
        netAddrWithTime.portShort = port;

        netAddrWithTime.pack();
        return netAddrWithTime;
    }

    //Constructors
    NetAddrWithTime() { }
    NetAddrWithTime(ByteBuffer payload) {
        this.payload = payload;
        this.unpack();
    }

    @Override
    public ByteBuffer pack() {
        //Pack into byte structures
        this.time = Converter.intToByteBuffer(this.timeInt).array();
        this.services = Converter.longToByteBuffer(this.servicesLong).array();
        this.address = Converter.stringAddressToBytes(this.addressString).array();
        this.port = Converter.shortToByteBufferBigEndian(this.portShort).array();

        //Create Payload
        this.payload = ByteBuffer.allocate(30)
                .order(ByteOrder.LITTLE_ENDIAN)
                .put(this.time).put(this.services).put(this.address).put(this.port)
                .flip();

        return this.payload;
    }

    @Override
    public void unpack() {
        //Unpack into byte structures
        this.time = Converter.intToByteBuffer(this.payload.getInt()).array();
        this.services = Converter.longToByteBuffer(this.payload.getLong()).array();
        this.address = Arrays.copyOfRange(this.payload.array(), this.payload.position(), this.payload.position() + 16);
        this.payload.position(this.payload.position() + 16);
        this.port = Converter.shortToByteBuffer(this.payload.getShort()).array();

        //Init Variables with unpacked byte structures
        this.timeInt = Converter.bytesToUInt32(this.time);
        this.servicesLong = Converter.bytesToUInt64(this.services);
        this.addressString = Converter.byteAddressToString(this.address);
        this.portShort = Converter.bytesToUInt16BigEndian(this.port);
    }

    @Override
    public void print(int tabsCount) {
        System.out.println(this.toString(tabsCount));
    }

    @Override
    public String toString(int tabsCount) {
        StringBuilder stringBuilder = new StringBuilder();
        String tabs = "\t".repeat(Math.max(0, tabsCount));

        stringBuilder.append(tabs).append("[ NetAddrWithTime ]").append("\n");
        stringBuilder.append(tabs).append("\tTIME: ").append(Arrays.toString(this.time)).append(" ( ").append(Converter.longTimestampToString((long) this.timeInt)).append(" )").append("\n");
        stringBuilder.append(tabs).append("\tSERVICES: ").append(Arrays.toString(this.services)).append(" ( ").append(this.servicesLong).append(" )").append("\n");
        stringBuilder.append(tabs).append("\tADDRESS: ").append(Arrays.toString(this.address)).append(" ( ").append(this.addressString).append(" ) ").append("\n");
        stringBuilder.append(tabs).append("\tPORT: ").append(Arrays.toString(this.port)).append(" ( ").append(this.portShort).append(" )").append("\n");
        stringBuilder.append(tabs).append("[ END: NetAddrWithTime ]").append("\n");

        return stringBuilder.toString();
    }
}
