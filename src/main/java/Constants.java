import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

public class Constants {
    public static final String DNS_FILE = "dns.txt";

    public static final Integer MAGIC =  0xD9B4BEF9;

    public final static Integer VERSION = 70015;
    public final static Integer SERVICES = 0;
    public final static String USER_AGENT = "BitLab 2.0";

    public static byte[] CCODES = new byte[] {
            0x01, 0x10, 0x11, 0x12, 0x40, 0x41, 0x42, 0x43
    };
    public static String[] CCODES_STRINGS = new String[] {
            "REJECT_MALFORMED", "REJECT_INVALID", "REJECT_OBSOLETE", "REJECT_DUPLICATE",
            "REJECT_NONSTANDARD", "REJECT_DUST", "REJECT_INSUFFICIENTFEE", "REJECT_CHECKPOINT"
    };
    public static String CCODE_UNKNOWN = "REJECT_UNKNOWN";

    public static final String[] AVAILABLE_COMMANDS = new String[] {
            "exit", "connect", "help", "ips",
            "peers", "read", "generate"
    };
    public static final String[] BITCOIN_COMMANDS = new String[] {
            "version",
            "verack",
            "ping",
            "pong",
            "getaddr",
            "addr",
            "reject",
            "getheaders",
            "headers",
            "getdata",
            "getblocks",
            "block",
            "inv",
            "tx",
    };

    public static String HELP_TEXT;

    static {
        try {
            List<String> list = Files.readAllLines(Paths.get("help.txt"));
            HELP_TEXT = String.join("\n", list);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
