import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.Random;

public class TxOut extends Payload {
    //Variables used to unpack
    long valueLong;
    VarInt pkScriptLengthVarInt;
    String pkScriptString;

    //Variables used to pack
    private byte[] value;
    private byte[] pk_script_length;
    private byte[] pk_script;

    //Getters
    public long getValueLong() { return this.valueLong; }
    public VarInt getPkScriptLengthVarInt() { return this.pkScriptLengthVarInt; }
    public String getPkScriptString() { return this.pkScriptString; }

    //Generate Test Case
    public static TxOut GenerateTest() {
        return TxOut.Generate(
                new Random().nextLong(),
                "pk_script_7pRgKlkC4zyai5HspmI99eUZRroeSiCw"
        );
    }

    //Generate
    public static TxOut Generate(long value, String pkScript) {
        TxOut txOut = new TxOut();
        txOut.valueLong = value;
        txOut.pkScriptLengthVarInt = VarInt.Generate(pkScript.length());
        txOut.pkScriptString = pkScript;

        txOut.pack();
        return txOut;
    }

    public TxOut() { }
    public TxOut(ByteBuffer payload) {
        this.payload = payload;
        this.unpack();
    }

    @Override
    public ByteBuffer pack() {
        //Pack into byte structures
        this.value = Converter.longToByteBuffer(this.valueLong).array();
        this.pk_script_length = this.pkScriptLengthVarInt.getPayload().array();
        this.pk_script = Converter.stringToBytes(this.pkScriptString, false).array();

        //Create Payload
        this.payload = ByteBuffer.allocate(this.value.length + this.pk_script_length.length + this.pk_script.length)
                .order(ByteOrder.LITTLE_ENDIAN)
                .put(this.value).put(pk_script_length).put(this.pk_script)
                .flip();

        return this.payload;
    }

    @Override
    public void unpack() {
        //VALUE
        this.value = Converter.longToByteBuffer(this.payload.getLong()).array();
        this.valueLong = Converter.bytesToUInt64(this.value);

        //PK_SCRIPT_LENGTH
        int length = VarInt.unpackLength(this.payload.get());
        this.payload.position(this.payload.position() - 1);
        this.pkScriptLengthVarInt = new VarInt(
                ByteBuffer.allocate(length).order(ByteOrder.LITTLE_ENDIAN)
                        .put(Arrays.copyOfRange(this.payload.array(), this.payload.position(), this.payload.position() + length))
                .flip()
        );
        this.pk_script_length = this.pkScriptLengthVarInt.pack().array();
        this.payload.position(this.payload.position() + length);

        //PK_SCRIPT
        int size = (int) this.pkScriptLengthVarInt.getValueLong();
        this.pk_script = Arrays.copyOfRange(this.payload.array(), this.payload.position(), this.payload.position() + size);
        this.pkScriptString = Converter.bytesToString(this.pk_script);
    }

    @Override
    public void print(int tabsCount) {
        System.out.println(this.toString(tabsCount));
    }

    @Override
    public String toString(int tabsCount) {
        StringBuilder stringBuilder = new StringBuilder();
        String tabs = "\t".repeat(Math.max(0, tabsCount));

        stringBuilder.append(tabs).append("[ TxOut ]").append("\n");
        stringBuilder.append(tabs).append("\tVALUE: ").append(Arrays.toString(this.value)).append(" ( ").append(this.valueLong).append(" )").append("\n");
        stringBuilder.append(tabs).append("\tPK_SCRIPT_LENGTH: ").append("\n");
        stringBuilder.append(this.pkScriptLengthVarInt.toString(tabsCount + 1));
        stringBuilder.append(tabs).append("\tPK_SCRIPT: ").append(Arrays.toString(this.pk_script)).append(" ( ").append(this.pkScriptString).append(" )").append("\n");
        stringBuilder.append(tabs).append("[ END: TxOut ]").append("\n");

        return stringBuilder.toString();
    }
}
