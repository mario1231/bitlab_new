import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;

public class Menu {
    private Peer peer;
    private Thread peerThread;

    private boolean onExit = false;

    public Menu(Peer peer) {
        this.peer = peer;
        peerThread = new Thread(peer, "peer");
        peerThread.start();
    }

    private void welcomeMessage() {
        System.out.println("Welcome to Bitlab!");
        System.out.println("Type 'help' for list of available commands or 'exit' to exit.");
        System.out.println();
    }

    private void parseCommand(String command) {
        String[] split = command.split("\\s");

        if (split.length < 1) {
            System.out.println("Unrecognized command.");
            return;
        }

        if (! Arrays.asList(Constants.AVAILABLE_COMMANDS).contains(split[0]) && ! Arrays.asList(Constants.BITCOIN_COMMANDS).contains(split[0])) {
            System.out.println("Unrecognized command.");
            return;
        }

        switch (split[0]) {
            case "connect":
                this.peer.connectToPeer();
                break;

            case "ips":
                this.peer.printAvailablePeers();
                break;

            case "peers":
                this.peer.printConnectedPeers();
                break;

            case "exit":
                this.onExit();
                break;

            case "help":
                this.help();
                break;

            case "read":
                this.peer.printUnreadMessages();
                break;

            case "generate": {
                if (split.length > 1) {
                    System.out.println("[ GENERATED ]");

                    switch (split[1]) {
                        case "version":
                            VersionPayload versionPayload = VersionPayload.GenerateTest();
                            versionPayload.print(0);
                            System.out.println("[ PARSED ]");
                            new VersionPayload(versionPayload.getPayload()).print(0);
                            break;
                        case "verack":
                            VerackPayload verackPayload = VerackPayload.GenerateTest();
                            verackPayload.print(0);
                            System.out.println("[ PARSED ]");
                            new VerackPayload(verackPayload.getPayload()).print(0);
                            break;
                        case "ping":
                        case "pong":
                            PingPongPayload pingPongPayload = PingPongPayload.GenerateTest();
                            pingPongPayload.print(0);
                            System.out.println("[ PARSED ]");
                            new PingPongPayload(pingPongPayload.getPayload()).print(0);
                            break;
                        case "getaddr":
                            GetAddrPayload getAddrPayload = GetAddrPayload.GenerateTest();
                            getAddrPayload.print(0);
                            System.out.println("[ PARSED ]");
                            new GetAddrPayload(getAddrPayload.getPayload()).print(0);
                            break;
                        case "addr":
                            AddrPayload addrPayload = AddrPayload.GenerateTest();
                            addrPayload.print(0);
                            System.out.println("[ PARSED ]");
                            new AddrPayload(addrPayload.getPayload()).print(0);
                            break;
                        case "reject":
                            RejectPayload rejectPayload = RejectPayload.GenerateTest();
                            rejectPayload.print(0);
                            System.out.println("[ PARSED ]");
                            new RejectPayload(rejectPayload.getPayload()).print(0);
                            break;
                        case "getheaders":
                        case "getblocks":
                            GetHeadersAndGetBlocksPayload getHeadersAndGetBlocksPayload = GetHeadersAndGetBlocksPayload.GenerateTest();
                            getHeadersAndGetBlocksPayload.print(0);
                            System.out.println("[ PARSED ]");
                            new GetHeadersAndGetBlocksPayload(getHeadersAndGetBlocksPayload.getPayload()).print(0);
                            break;
                        case "headers":
                            HeadersPayload headersPayload = HeadersPayload.GenerateTest();
                            headersPayload.print(0);
                            System.out.println("[ PARSED ]");
                            new HeadersPayload(headersPayload.getPayload()).print(0);
                            break;
                        case "getdata":
                        case "inv":
                            GetDataAndInvPayload getDataAndInvPayload = GetDataAndInvPayload.GenerateTest();
                            getDataAndInvPayload.print(0);
                            System.out.println("[ PARSED ]");
                            new GetDataAndInvPayload(getDataAndInvPayload.getPayload()).print(0);
                            break;
                        case "tx":
                            TxPayload txPayload = TxPayload.GenerateTest();
                            txPayload.print(0);
                            System.out.println("[ PARSED ]");
                            new TxPayload(txPayload.getPayload()).print(0);
                            break;
                        case "block":
                            BlockPayload blockPayload = BlockPayload.GenerateTest();
                            blockPayload.print(0);
                            System.out.println("[ PARSED ]");
                            new BlockPayload(blockPayload.getPayload()).print(0);
                            break;
                    }

                    System.out.println("[ END: GENERATED / PARSED ]");
                }
            } break;

            default:
                if (split.length == 1) {
                    if (Arrays.asList(Constants.BITCOIN_COMMANDS).contains(split[0]))
                        peer.sendMessage(command, null);
                } else {
                    if (Arrays.asList(Constants.BITCOIN_COMMANDS).contains(split[0]))
                        peer.sendMessage(split[0], Arrays.copyOfRange(split, 1, split.length));
                }
                break;
        }
    }

    public void prompt() {
        System.out.print("Bitlab > ");
    }

    private void onExit() {
        this.onExit = true;
        peer.onExit();

        System.out.println("Exiting. Thanks for using Bitlab!");
    }

    public void run() {
        welcomeMessage();

        Scanner scanner = new Scanner(System.in);
        String command;

        do {
            this.prompt();
            command = scanner.nextLine().trim().toLowerCase();
            this.parseCommand(command);
        } while (!this.onExit);
    }

    public void help() {
        System.out.println(Constants.HELP_TEXT);
    }
}
