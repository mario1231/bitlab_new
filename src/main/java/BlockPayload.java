import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;

public class BlockPayload extends Payload {
    //Variables used to unpack
    BlockHeader blockHeader;
    ArrayList<TxPayload> txList;

    //Variables used to pack
    byte[] block_header;
    byte[] txns;

    //Getters
    public BlockHeader getBlockHeader() { return this.blockHeader; }
    public ArrayList<TxPayload> getTxList() { return this.txList; }

    //Generate Test Case
    public static BlockPayload GenerateTest() {
        BlockHeader blockHeader = BlockHeader.GenerateTest();
        VarInt txnCount = blockHeader.getTxnCountVarInt();
        ArrayList<TxPayload> list = new ArrayList<>();

        for (int i = 0; i < txnCount.getValueLong(); ++i)
            list.add(TxPayload.GenerateTest());

        return BlockPayload.Generate( blockHeader, list );
    }

    //Generate
    public static BlockPayload Generate(BlockHeader blockHeader, ArrayList<TxPayload> txList) {
        BlockPayload blockPayload = new BlockPayload();
        blockPayload.blockHeader = blockHeader;
        blockPayload.txList = txList;

        blockPayload.pack();
        return blockPayload;
    }

    public BlockPayload() { }
    public BlockPayload(ByteBuffer payload) {
        this.payload = payload;
        this.unpack();
    }

    @Override
    public ByteBuffer pack() {
        //Helper Variables
        ByteBuffer txByteBuffer;

        //Pack into byte structures
        this.block_header = this.blockHeader.getPayload().array();

        int length = 0;
        for (TxPayload txPayload : this.txList)
            length += txPayload.getPayload().capacity();

        txByteBuffer = ByteBuffer.allocate(length).order(ByteOrder.LITTLE_ENDIAN);

        for (TxPayload txPayload : this.txList)
            txByteBuffer.put(txPayload.getPayload());

        this.txns = txByteBuffer.array();

        this.payload = ByteBuffer.allocate(this.block_header.length + this.txns.length).order(ByteOrder.LITTLE_ENDIAN)
                .put(this.block_header).put(this.txns)
                .flip();

        return this.payload;
    }

    @Override
    public void unpack() {
        //Helper Variables
        byte[] headerBytes = new byte[80];
        byte[] varIntBytes;
        byte[] bytes;
        VarInt varInt;

        //Init Variables
        this.txList = new ArrayList<>();

        //Unpack into byte structures
        //HEADER
        this.payload.get(headerBytes);
        int length = VarInt.unpackLength(this.payload.get());
        this.payload.position(this.payload.position() - 1);
        varIntBytes = new byte[length];
        this.payload.get(varIntBytes);
        varInt = new VarInt(ByteBuffer.allocate(length).order(ByteOrder.LITTLE_ENDIAN).put(varIntBytes).flip());

        this.block_header = ByteBuffer.allocate(80 + length)
                .order(ByteOrder.LITTLE_ENDIAN)
                .put(headerBytes).put(varIntBytes).array();
        this.blockHeader = new BlockHeader(
                ByteBuffer.allocate(this.block_header.length).order(ByteOrder.LITTLE_ENDIAN)
                .put(this.block_header).flip()
        );

        //TXNS
        this.txns = new byte[this.payload.remaining()];
        this.payload.get(this.txns);
        this.payload.position(this.payload.position() - this.txns.length);

        for (int i = 0; i < varInt.getValueLong(); ++i) {
            boolean isFlagSet = false;
            int totalTxLength = 0;

            //VERSION
            this.payload.position(this.payload.position() + 4);
            totalTxLength += 4;

            //FLAG
            if (this.payload.get() == 0) {
                isFlagSet = true;
                this.payload.position(this.payload.position() + 1);
                totalTxLength += 2;
            } else {
                this.payload.position(this.payload.position() - 1);
            }

            //TX_IN
            length = VarInt.unpackLength(this.payload.get());
            this.payload.position(this.payload.position() - 1);
            bytes = new byte[length];
            this.payload.get(bytes);
            int txCount = (int) new VarInt(ByteBuffer.allocate(bytes.length).order(ByteOrder.LITTLE_ENDIAN).put(bytes).flip()).getValueLong();
            totalTxLength += length;

            for (int j = 0; j < txCount; ++j) {
                this.payload.position(this.payload.position() + 36);
                totalTxLength += 36;

                length = VarInt.unpackLength(this.payload.get());
                this.payload.position(this.payload.position() - 1);
                bytes = new byte[length];
                this.payload.get(bytes);
                totalTxLength += length;

                int scriptLength = (int) new VarInt(ByteBuffer.allocate(bytes.length).order(ByteOrder.LITTLE_ENDIAN).put(bytes).flip()).getValueLong();
                this.payload.position(this.payload.position() + scriptLength);
                totalTxLength += scriptLength;

                //SEQUENCE
                this.payload.position(this.payload.position() + 4);
                totalTxLength += 4;
            }

            //TX_OUT
            length = VarInt.unpackLength(this.payload.get());
            this.payload.position(this.payload.position() - 1);
            bytes = new byte[length];
            this.payload.get(bytes);
            txCount = (int) new VarInt(ByteBuffer.allocate(bytes.length).order(ByteOrder.LITTLE_ENDIAN).put(bytes).flip()).getValueLong();
            totalTxLength += length;

            for (int j = 0; j < txCount; ++j) {
                this.payload.position(this.payload.position() + 8);
                totalTxLength += 8;

                length = VarInt.unpackLength(this.payload.get());
                this.payload.position(this.payload.position() - 1);
                bytes = new byte[length];
                this.payload.get(bytes);
                totalTxLength += length;

                int scriptLength = (int) new VarInt(ByteBuffer.allocate(bytes.length).order(ByteOrder.LITTLE_ENDIAN).put(bytes).flip()).getValueLong();
                this.payload.position(this.payload.position() + scriptLength);
                totalTxLength += scriptLength;
            }

            if (isFlagSet) {
                //TX_WITNESSES
                length = VarInt.unpackLength(this.payload.get());
                this.payload.position(this.payload.position() - 1);
                bytes = new byte[length];
                this.payload.get(bytes);
                txCount = (int) new VarInt(ByteBuffer.allocate(bytes.length).order(ByteOrder.LITTLE_ENDIAN).put(bytes).flip()).getValueLong();
                totalTxLength += length;

                for (int j = 0; j < txCount; ++j) {
                    length = VarInt.unpackLength(this.payload.get());
                    this.payload.position(this.payload.position() - 1);
                    bytes = new byte[length];
                    this.payload.get(bytes);
                    totalTxLength += length;
                    int dataCount = (int) new VarInt(ByteBuffer.allocate(bytes.length).order(ByteOrder.LITTLE_ENDIAN).put(bytes).flip()).getValueLong();
                    this.payload.position(this.payload.position() + dataCount);
                    totalTxLength += dataCount;
                }
            }

            //LOCK_TIME
            this.payload.position(this.payload.position() + 4);
            totalTxLength += 4;

            byte[] txBytes = new byte[totalTxLength];
            this.payload.position(this.payload.position() - totalTxLength);
            this.payload.get(txBytes);

            TxPayload txPayload = new TxPayload(ByteBuffer.allocate(totalTxLength).order(ByteOrder.LITTLE_ENDIAN).put(txBytes).flip());
            this.txList.add(txPayload);
        }
    }

    @Override
    public void print(int tabsCount) {
        System.out.println(this.toString(tabsCount));
    }

    @Override
    public String toString(int tabsCount) {
        StringBuilder stringBuilder = new StringBuilder();
        String tabs = "\t".repeat(Math.max(0, tabsCount));

        stringBuilder.append(tabs).append("[ Block Payload ]").append("\n").append("\n");
        stringBuilder.append(tabs).append("\tBLOCK_HEADER: ").append("\n");
        stringBuilder.append(this.blockHeader.toString(tabsCount + 1));
        stringBuilder.append(tabs).append("\tTXNS: ( total : ").append(this.txList.size()).append(" )").append("\n");
        for (int i = 0; i < Math.min(5, this.txList.size()); ++i)
            stringBuilder.append(this.txList.get(i).toString(tabsCount + 1));
        stringBuilder.append(tabs).append("[ END: Block Payload ]").append("\n");

        return stringBuilder.toString();
    }
}
