import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;

public class GetHeadersAndGetBlocksPayload extends Payload {
    //Variables used to unpack
    private int versionInt;
    private VarInt hashCountVarInt;
    private ArrayList<String> blockLocationHashesList;
    private String hashStopString;

    //Variables used to pack
    protected byte[] version;
    protected byte[] hash_count;
    protected byte[] block_locator_hashes;
    protected byte[] hash_stop;

    //Getters
    public int getVersionInt() { return this.versionInt; }
    public VarInt getHashCountVarInt() { return this.hashCountVarInt; }
    public ArrayList<String> getBlockLocationHashesList() { return this.blockLocationHashesList; }
    public String getHashStopString() { return this.hashStopString; }

    //Generate Test Case
    public static GetHeadersAndGetBlocksPayload GenerateTest() {
        ArrayList<String> list = new ArrayList<>();
        list.add("LYtAOvCf99T5tkNwM5n8Ou7nAqMJK4ao");
        list.add("ApoGbgQdLgDEaPzjcKC0ExHb65FYGeRp");
        list.add("r2POySKsphqTHHuSHene70A0exD8IjCB");
        list.add("sHB0ahbIeqPscyzwPNW8W1UXEH0jE6CX");
        list.add("LijCdMuvnQfRcvfdndRJ4OLHkcRIorug");

        return GetHeadersAndGetBlocksPayload.Generate(
                Constants.VERSION,
                list,
                "\1".repeat(32)
        );
    }

    //Generate
    public static GetHeadersAndGetBlocksPayload Generate(int version, ArrayList<String> hashes, String hashStop) {
        GetHeadersAndGetBlocksPayload getHeadersAndGetBlocksPayload = new GetHeadersAndGetBlocksPayload();

        getHeadersAndGetBlocksPayload.versionInt = version;
        getHeadersAndGetBlocksPayload.hashCountVarInt = VarInt.Generate(hashes.size());
        getHeadersAndGetBlocksPayload.blockLocationHashesList = hashes;
        getHeadersAndGetBlocksPayload.hashStopString = hashStop;

        getHeadersAndGetBlocksPayload.pack();
        return getHeadersAndGetBlocksPayload;
    }

    public GetHeadersAndGetBlocksPayload() { }
    public GetHeadersAndGetBlocksPayload(ByteBuffer payload) {
        this.payload = payload;
        this.unpack();
    }

    @Override
    public ByteBuffer pack() {
        //Helper Variables
        ByteBuffer byteBuffer = ByteBuffer.allocate(32 * this.blockLocationHashesList.size()).order(ByteOrder.LITTLE_ENDIAN);
        for (String s : this.blockLocationHashesList) {
            System.out.println(Converter.stringToBytes(s, false).array().length);
            byteBuffer.put(Converter.stringToBytes(s.substring(0, Math.min(s.length(), 32)), false).array());
        }

        //Pack into byte structures
        this.version = Converter.intToByteBuffer(Constants.VERSION).array();
        this.hash_count = hashCountVarInt.getPayload().array();
        this.block_locator_hashes = byteBuffer.array();
        this.hash_stop = Converter.stringToBytes(this.hashStopString, false).array();

        //Create Payload
        this.payload = ByteBuffer.allocate(this.version.length + this.hash_count.length + this.block_locator_hashes.length + this.hash_stop.length)
                .order(ByteOrder.LITTLE_ENDIAN)
                .put(this.version).put(this.hash_count).put(this.block_locator_hashes).put(this.hash_stop)
                .flip();

        return this.payload;
    }

    @Override
    public void unpack() {
        //Helper Variables
        ByteBuffer byteBuffer;
        byte[] bytes = new byte[32];

        //Init Variables
        this.blockLocationHashesList = new ArrayList<>();

        //Version
        this.version = Converter.intToByteBuffer(this.payload.getInt()).array();
        this.versionInt = Converter.bytesToUInt32(this.version);

        //HASH_COUNT
        int length = VarInt.unpackLength(this.payload.get());
        this.payload.position(this.payload.position() - 1);
        this.hashCountVarInt = new VarInt(
                ByteBuffer.allocate(length).order(ByteOrder.LITTLE_ENDIAN)
                        .put(Arrays.copyOfRange(this.payload.array(), this.payload.position(), this.payload.position() + length))
                .flip()
        );
        this.hash_count = this.hashCountVarInt.getPayload().array();
        this.payload.position(this.payload.position() + length);

        //HASHES
        byteBuffer = ByteBuffer.allocate(32 * (int)hashCountVarInt.getValueLong()).order(ByteOrder.LITTLE_ENDIAN);

        for (int i = 0; i < this.hashCountVarInt.getValueLong(); ++i) {
            this.payload.get(bytes);
            this.blockLocationHashesList.add(Converter.bytesToString(bytes));
            byteBuffer.put(bytes);
        }

        this.block_locator_hashes = byteBuffer.array();

        //HASH_STOP
        this.payload.get(bytes);
        this.hash_stop = bytes;
        this.hashStopString = Converter.bytesToString(this.hash_stop);
    }

    @Override
    public void print(int tabsCount) {
        System.out.println(this.toString(tabsCount));
    }

    @Override
    public String toString(int tabsCount) {
        StringBuilder stringBuilder = new StringBuilder();
        String tabs = "\t".repeat(Math.max(0, tabsCount));

        stringBuilder.append(tabs).append("[ GetHeaders / GetBlocks Payload ]").append("\n");
        stringBuilder.append(tabs).append("\tVERSION: ").append(Arrays.toString(this.version)).append(" ( ").append(this.versionInt).append(" )").append("\n");
        stringBuilder.append(tabs).append("\tHASH_COUNT:").append("\n");
        stringBuilder.append(this.hashCountVarInt.toString(tabsCount + 1));
        stringBuilder.append(tabs).append("\tBLOCK_LOCATOR_HASHES: ").append(Arrays.toString(this.blockLocationHashesList.toArray())).append("\n");
        stringBuilder.append(tabs).append("\tHASH_STOP: ").append(Arrays.toString(this.hash_stop)).append(" ( ").append(this.hashStopString).append(" )").append("\n");
        stringBuilder.append(tabs).append("[ END: GetHeaders / GetBlocks Payload ]").append("\n");

        return stringBuilder.toString();
    }
}
