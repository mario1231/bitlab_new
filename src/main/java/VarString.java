import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.Random;

public class VarString extends Payload{
    //Variables used to unpack
    private VarInt lengthVarInt;
    private String stringString;

    //Variables used to unpack
    byte[] var_int;
    byte[] string;

    //Getters
    public VarInt getLengthVarInt() { return this.lengthVarInt; }
    public String getStringString() { return this.stringString; }

    public static VarString GenerateTest() {
        return VarString.Generate("Dichlorodifenylotricholoretan_" + new Random().nextLong());
    }

    public static VarString Generate(String s) {
        VarString varString = new VarString();
        varString.lengthVarInt = VarInt.Generate(s.length());
        varString.stringString = s;

        varString.pack();
        return varString;
    }

    public VarString() { }
    public VarString(ByteBuffer payload) {
        this.payload = payload;
        this.unpack();
    }

    @Override
    public ByteBuffer pack() {
        //Pack into byte structures
        this.var_int = lengthVarInt.getPayload().array();
        this.string = Converter.stringToBytes(this.stringString, false).array();

        this.payload = ByteBuffer.allocate(this.var_int.length + this.string.length)
                .order(ByteOrder.LITTLE_ENDIAN)
                .put(this.var_int).put(this.string)
                .flip();

        return payload;
    }


    @Override
    public void unpack() {
        int varIntLength = VarInt.unpackLength(this.payload.array()[0]);
        this.var_int = Arrays.copyOfRange(this.payload.array(), 0, varIntLength);
        this.string = Arrays.copyOfRange(this.payload.array(), varIntLength, this.payload.array().length);

        this.lengthVarInt = new VarInt(
                ByteBuffer.allocate(this.var_int.length).order(ByteOrder.LITTLE_ENDIAN).put(this.var_int).flip()
        );
        this.stringString = Converter.bytesToString(this.string);
    }

    @Override
    public void print(int tabsCount) {
        System.out.println(this.toString(tabsCount));
    }

    @Override
    public String toString(int tabsCount) {
        StringBuilder stringBuilder = new StringBuilder();
        String tabs = "\t".repeat(Math.max(0, tabsCount));

        stringBuilder.append(tabs).append("[ VarString ]").append("\n");
        stringBuilder.append(this.lengthVarInt.toString(tabsCount + 1));
        stringBuilder.append(tabs).append("\tSTRING: ").append(Arrays.toString(this.string)).append(" ( ").append(this.stringString).append(" )").append("\n");
        stringBuilder.append(tabs).append("[ END: VarString ]").append("\n");

        return stringBuilder.toString();
    }
}
