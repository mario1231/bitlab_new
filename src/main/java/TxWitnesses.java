import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;

public class TxWitnesses extends Payload {
    //Variables used to unpack
    private VarInt countVarInt;
    private ArrayList<TxWitness> txWitnessesList;

    //Variables used to pack
    private byte[] count;
    private byte[] witnesses;

    //Getters
    public VarInt getCountVarInt() { return this.countVarInt; }
    public ArrayList<TxWitness> getTxWitnessesList() { return this.txWitnessesList; }

    //Generate Test Case
    public static TxWitnesses GenerateTest() {
        ArrayList<TxWitness> list = new ArrayList<>();
        for (int i = 0; i < 5; ++i)
            list.add(TxWitness.GenerateTest());
        return TxWitnesses.Generate(list);
    }

    //Generate
    public static TxWitnesses Generate(ArrayList<TxWitness> list) {
        TxWitnesses txWitnesses = new TxWitnesses();
        txWitnesses.countVarInt = VarInt.Generate(list.size());
        txWitnesses.txWitnessesList = list;

        txWitnesses.pack();
        return txWitnesses;
    }

    public TxWitnesses() { }
    public TxWitnesses(ByteBuffer payload) {
        this.payload = payload;
        this.unpack();
    }

    @Override
    public ByteBuffer pack() {
        this.count = this.countVarInt.getPayload().array();

        int size = 0;
        for (TxWitness tx : this.txWitnessesList)
            size += tx.pack().capacity();

        ByteBuffer byteBuffer = ByteBuffer.allocate(size).order(ByteOrder.LITTLE_ENDIAN);

        for (TxWitness tx : this.txWitnessesList)
            byteBuffer.put(tx.getPayload().array());

        this.witnesses = byteBuffer.array();

        this.payload = ByteBuffer.allocate(this.count.length + this.witnesses.length)
                .order(ByteOrder.LITTLE_ENDIAN)
                .put(this.count).put(this.witnesses)
                .flip();

        return this.payload;
    }

    @Override
    public void unpack() {
        this.txWitnessesList = new ArrayList<>();

        //COUNT
        int length = VarInt.unpackLength(this.payload.get());
        this.payload.position(this.payload.position() - 1);
        this.countVarInt = new VarInt(
                ByteBuffer.allocate(length).order(ByteOrder.LITTLE_ENDIAN)
                        .put(Arrays.copyOfRange(this.payload.array(), this.payload.position(), this.payload.position() + length))
                .flip()
        );
        this.count = this.countVarInt.pack().array();
        this.payload.position(this.payload.position() + length);

        //WITNESSES_LIST
        int txCount = (int) this.countVarInt.getValueLong();
        for (int i = 0; i < txCount; ++i) {
            int length2 = VarInt.unpackLength(this.payload.get());
            this.payload.position(this.payload.position() - 1);
            VarInt varInt = new VarInt(
                    ByteBuffer.allocate(length).order(ByteOrder.LITTLE_ENDIAN)
                            .put(Arrays.copyOfRange(this.payload.array(), this.payload.position(), this.payload.position() + length2))
                    .flip()
            );
            int txWitnessLength = (int) varInt.getValueLong();

            ByteBuffer byteBuffer = ByteBuffer.allocate(length2 + txWitnessLength).order(ByteOrder.LITTLE_ENDIAN)
                    .put(Arrays.copyOfRange(this.payload.array(), this.payload.position(), this.payload.position() + length2 + txWitnessLength))
                    .flip();
            TxWitness txWitness = new TxWitness(byteBuffer);
            this.txWitnessesList.add(txWitness);
            this.payload.position(this.payload.position() + length2 + txWitnessLength);
        }
        int totalLength = 0;
        for (TxWitness txWitness : this.txWitnessesList)
            totalLength += txWitness.getPayload().capacity();

        ByteBuffer listBuffer = ByteBuffer.allocate(totalLength).order(ByteOrder.LITTLE_ENDIAN);
        for (TxWitness txWitness : this.txWitnessesList)
            listBuffer.put(txWitness.getPayload().array());

        this.witnesses = listBuffer.array();
    }

    @Override
    public void print(int tabsCount) {
        System.out.println(this.toString(tabsCount));
    }

    @Override
    public String toString(int tabsCount) {
        StringBuilder stringBuilder = new StringBuilder();
        String tabs = "\t".repeat(Math.max(0, tabsCount));

        stringBuilder.append(tabs).append("[ TxWitnesses ]").append("\n");
        stringBuilder.append(tabs).append("\tCOUNT: ").append("\n");
        stringBuilder.append(this.countVarInt.toString(tabsCount + 1));
        stringBuilder.append(tabs).append("\tWITNESSES: ( total : ").append(this.txWitnessesList.size()).append(" )").append("\n");
        for (int i = 0; i < Math.min(5, this.txWitnessesList.size()); ++i)
            stringBuilder.append(this.txWitnessesList.get(i).toString(tabsCount + 1));
        stringBuilder.append(tabs).append("[ END: TxWitnesses ]").append("\n");

        return stringBuilder.toString();
    }
}
