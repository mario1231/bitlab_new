import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.Random;

public class Outpoint extends Payload {
    //Variables used to unpack
    private String hashString;
    private int indexInt;

    //Variables used to pack
    private byte[] hash;
    private byte[] index;

    //Getters
    public String getHashString() { return this.hashString; }
    public int getIndexInt() { return this.indexInt; }

    //Generate Test Case
    public static Outpoint GenerateTest() {
        return Outpoint.Generate(
            "4iRyn3WatslzXXzWfQAdWOQu2lSrsIat",
            new Random().nextInt()
        );
    }

    //Generate
    public static Outpoint Generate(String hash, int index) {
        Outpoint outpoint = new Outpoint();
        outpoint.hashString = hash;
        outpoint.indexInt = index;

        outpoint.pack();
        return outpoint;
    }

    public Outpoint() { }
    public Outpoint(ByteBuffer payload) {
        this.payload = payload;
        this.unpack();
    }

    @Override
    public ByteBuffer pack() {
        //Pack into byte structures
        this.hash = Converter.stringToBytes(this.hashString, false).array();
        this.index = Converter.intToByteBuffer(this.indexInt).array();

        //Create Payload
        this.payload = ByteBuffer.allocate(this.hash.length + this.index.length)
                .order(ByteOrder.LITTLE_ENDIAN)
                .put(this.hash).put(this.index)
                .flip();

        return this.payload;
    }

    @Override
    public void unpack() {
        //Unpack into byte structures
        this.hash = Arrays.copyOfRange(this.payload.array(), 0, 32);
        this.index = Arrays.copyOfRange(this.payload.array(), 32, 36);

        //Init Variables with unpacked byte structures
        this.hashString = Converter.bytesToString(this.hash);
        this.indexInt = Converter.bytesToUInt32(this.index);
    }

    @Override
    public void print(int tabsCount) {
        System.out.println(this.toString(tabsCount));
    }

    @Override
    public String toString(int tabsCount) {
        StringBuilder stringBuilder = new StringBuilder();
        String tabs = "\t".repeat(Math.max(0, tabsCount));

        stringBuilder.append(tabs).append("[ Outpoint ]").append("\n");
        stringBuilder.append(tabs).append("\tHASH: ").append(Arrays.toString(this.hash)).append(" ( ").append(this.hashString).append(" )").append("\n");
        stringBuilder.append(tabs).append("\tINDEX: ").append(Arrays.toString(this.index)).append(" ( ").append(this.indexInt).append(" )").append("\n");
        stringBuilder.append(tabs).append("[ END: Outpoint ]").append("\n");

        return stringBuilder.toString();
    }
}
