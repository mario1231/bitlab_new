import java.nio.ByteBuffer;

abstract public class Payload {
    protected ByteBuffer payload;

    public ByteBuffer getPayload() { return this.payload; }

    public abstract ByteBuffer pack();
    public abstract void unpack();
    public abstract void print(int tabsCount);

    public abstract String toString(int tabsCount);
}
