import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class TxPayload extends Payload {
    //Variables used to unpack
    int versionInt;
    short flagShort;
    VarInt txInCountVarInt;
    ArrayList<TxIn> txInList;
    VarInt txOutCountVarInt;
    ArrayList<TxOut> txOutList;
    TxWitnesses txWitnesses;
    int locktimeInt;

    //Variables used to pack
    byte[] version;
    byte[] flag;
    byte[] tx_in_count;
    byte[] tx_in;
    byte[] tx_out_count;
    byte[] tx_out;
    byte[] tx_witnesses;
    byte[] locktime;

    //Getters
    public int getVersionInt() { return this.versionInt; }
    public short getFlagShort() { return this.flagShort; }
    public VarInt getTxInCountVarInt() { return this.txInCountVarInt; }
    public ArrayList<TxIn> getTxInList() { return this.txInList; }
    public VarInt getTxOutCountVarInt() { return this.txOutCountVarInt; }
    public ArrayList<TxOut> getTxOutList() { return this.txOutList; }
    public TxWitnesses getTxWitnesses() { return this.txWitnesses; }
    public int getLocktimeInt() { return this.locktimeInt; }

    //Generate Test Case
    public static TxPayload GenerateTest() {
        ArrayList<TxIn> txInList = new ArrayList<>();
        ArrayList<TxOut> txOutList = new ArrayList<>();

        for (int i = 0; i < 5; ++i) {
            txInList.add(TxIn.GenerateTest());
            txOutList.add(TxOut.GenerateTest());
        }

        return TxPayload.Generate(
                Constants.VERSION,
                (short) 1,
                txInList,
                txOutList,
                TxWitnesses.GenerateTest(),
                new Random().nextInt()
        );
    }

    //Generate
    public static TxPayload Generate(int version, short flag, ArrayList<TxIn> txIn, ArrayList<TxOut> txOut, TxWitnesses txWitnesses, int locktime) {
        TxPayload txPayload = new TxPayload();
        txPayload.versionInt = version;
        txPayload.flagShort = flag;
        txPayload.txInCountVarInt = VarInt.Generate(txIn.size());
        txPayload.txInList = txIn;
        txPayload.txOutCountVarInt = VarInt.Generate(txOut.size());
        txPayload.txOutList = txOut;
        txPayload.txWitnesses = txWitnesses;
        txPayload.locktimeInt = locktime;

        txPayload.pack();
        return txPayload;
    }

    public TxPayload() { }
    public TxPayload(ByteBuffer payload) {
        this.payload = payload;
        this.unpack();
    }

    @Override
    public ByteBuffer pack() {
        //Helper Variables
        int txLength = 0;
        for (TxIn tx : this.txInList)
            txLength += tx.getPayload().capacity();

        ByteBuffer byteBuffer = ByteBuffer.allocate(txLength).order(ByteOrder.LITTLE_ENDIAN);
        for (TxIn tx : this.txInList) {
            byteBuffer.put(tx.getPayload().array());
        }

        txLength = 0;
        for (TxOut tx : this.txOutList)
            txLength += tx.getPayload().capacity();

        ByteBuffer byteBuffer2 = ByteBuffer.allocate(txLength).order(ByteOrder.LITTLE_ENDIAN);
        for (TxOut tx : this.txOutList)
            byteBuffer2.put(tx.getPayload().array());

        //Pack into byte structures
        this.version = Converter.intToByteBuffer(this.versionInt).array();
        if (this.flagShort != 0)
            this.flag = Converter.shortToByteBufferBigEndian(this.flagShort).array();
        this.tx_in_count = this.txInCountVarInt.getPayload().array();
        this.tx_in = byteBuffer.array();
        this.tx_out_count = this.txOutCountVarInt.getPayload().array();
        this.tx_out = byteBuffer2.array();
        if (this.flagShort != 0)
            this.tx_witnesses = this.txWitnesses.getPayload().array();
        this.locktime = Converter.intToByteBuffer(this.locktimeInt).array();

        //Create Payload
        if (this.flagShort != 0) {
            this.payload = ByteBuffer.allocate(
                    this.version.length + this.flag.length + this.tx_in_count.length + this.tx_in.length +
                            this.tx_out_count.length + this.tx_out.length + this.tx_witnesses.length + this.locktime.length
            ).order(ByteOrder.LITTLE_ENDIAN)
                    .put(this.version).put(this.flag).put(this.tx_in_count).put(this.tx_in)
                    .put(this.tx_out_count).put(this.tx_out).put(this.tx_witnesses).put(this.locktime)
                    .flip();
        } else {
            this.payload = ByteBuffer.allocate(
                    this.version.length + this.tx_in_count.length + this.tx_in.length +
                            this.tx_out_count.length + this.tx_out.length  + this.locktime.length
            ).order(ByteOrder.LITTLE_ENDIAN)
                    .put(this.version).put(this.tx_in_count).put(this.tx_in)
                    .put(this.tx_out_count).put(this.tx_out).put(this.locktime)
                    .flip();
        }

        return this.payload;
    }

    @Override
    public void unpack() {
        //Helper Variables
        int length;
        int count;
        byte[] bytes = new byte[41];
        byte[] bytes2 = new byte[9];
        ByteBuffer byteBuffer;

        //Init Variables
        this.txInList = new ArrayList<>();
        this.txOutList = new ArrayList<>();

        //VERSION
        this.version = Converter.intToByteBuffer(this.payload.order(ByteOrder.LITTLE_ENDIAN).getInt()).array();
        this.versionInt = Converter.bytesToUInt32(this.version);

        //FLAG
        if (this.payload.get() == 0) {
            this.payload.position(this.payload.position() - 1);
            this.flagShort = this.payload.getShort();
            this.flag = Converter.shortToByteBuffer(this.flagShort).array();
        } else {
            this.flagShort = 0;
            this.payload.position(this.payload.position() - 1);
        }

        //TX_IN_COUNT
        length = VarInt.unpackLength(this.payload.get());
        this.payload.position(this.payload.position() - 1);
        this.txInCountVarInt = new VarInt(
                ByteBuffer.allocate(length).order(ByteOrder.LITTLE_ENDIAN)
                        .put(Arrays.copyOfRange(this.payload.array(), this.payload.position(), this.payload.position() + length))
                        .flip()
        );
        this.tx_in_count = this.txInCountVarInt.getPayload().array();
        this.payload.position(this.payload.position() + length);

        //TX_IN
        count = (int)txInCountVarInt.getValueLong();
        int totalTxLength = 0;

        for (int i = 0; i < count; ++i) {
            this.payload.position(this.payload.position() + 36);
            length = VarInt.unpackLength(this.payload.get());
            this.payload.position(this.payload.position() - 1);
            VarInt varInt = new VarInt(
                    ByteBuffer.allocate(length).order(ByteOrder.LITTLE_ENDIAN)
                            .put(Arrays.copyOfRange(this.payload.array(), this.payload.position(), this.payload.position() + length))
                            .flip()
            );
            this.payload.position(this.payload.position() - 36);

            int txLength = 40 + length + (int) varInt.getValueLong();
            totalTxLength += txLength;

            byte[] bytesTx = new byte[txLength];
            this.payload.get(bytesTx);
            this.txInList.add(new TxIn(
                    ByteBuffer.allocate(txLength).order(ByteOrder.LITTLE_ENDIAN).put(bytesTx).flip()
            ));
        }

        byteBuffer = ByteBuffer.allocate(totalTxLength).order(ByteOrder.LITTLE_ENDIAN);
        for (TxIn txIn : this.txInList)
            byteBuffer.put(txIn.getPayload().array());
        this.tx_in = byteBuffer.array();

        //TX_OUT_COUNT
        length = VarInt.unpackLength(this.payload.get());
        this.payload.position(this.payload.position() - 1);
        this.txOutCountVarInt = new VarInt(
                ByteBuffer.allocate(length).order(ByteOrder.LITTLE_ENDIAN)
                        .put(Arrays.copyOfRange(this.payload.array(), this.payload.position(), this.payload.position() + length))
                        .flip()
        );
        this.tx_out_count = this.txOutCountVarInt.getPayload().array();
        this.payload.position(this.payload.position() + length);

        //TX_OUT
        count = (int)txOutCountVarInt.getValueLong();
        totalTxLength = 0;

        for (int i = 0; i < count; ++i) {
            this.payload.position(this.payload.position() + 8);
            length = VarInt.unpackLength(this.payload.get());
            this.payload.position(this.payload.position() - 1);
            VarInt varInt = new VarInt(
                    ByteBuffer.allocate(length).order(ByteOrder.LITTLE_ENDIAN)
                            .put(Arrays.copyOfRange(this.payload.array(), this.payload.position(), this.payload.position() + length))
                            .flip()
            );
            this.payload.position(this.payload.position() - 8);

            int txLength = 8 + length + (int) varInt.getValueLong();
            totalTxLength += txLength;

            byte[] bytesTx = new byte[txLength];
            this.payload.get(bytesTx);
            this.txOutList.add(new TxOut(
                    ByteBuffer.allocate(txLength).order(ByteOrder.LITTLE_ENDIAN).put(bytesTx).flip()
            ));
        }

        byteBuffer = ByteBuffer.allocate(totalTxLength).order(ByteOrder.LITTLE_ENDIAN);
        for (TxOut txOut : this.txOutList)
            byteBuffer.put(txOut.getPayload().array());
        this.tx_out = byteBuffer.array();

        //TX_WITNESSES
        if (this.flagShort != 0) {
            byte[] bytes3 = new byte[this.payload.remaining() - 4];
            this.payload.get(bytes3);
            this.tx_witnesses = bytes3;
            this.txWitnesses = new TxWitnesses(ByteBuffer.allocate(bytes3.length).order(ByteOrder.LITTLE_ENDIAN).put(bytes3).flip());
        }

        //LOCKTIME
        this.locktimeInt = this.payload.getInt();
        this.locktime = Converter.intToByteBuffer(this.locktimeInt).array();
    }

    @Override
    public void print(int tabsCount) {
        System.out.println(this.toString(tabsCount));
    }

    @Override
    public String toString(int tabsCount) {
        StringBuilder stringBuilder = new StringBuilder();
        String tabs = "\t".repeat(Math.max(0, tabsCount));

        stringBuilder.append(tabs).append("[ Tx Payload ]").append("\n");
        stringBuilder.append(tabs).append("\tVERSION: ").append(Arrays.toString(this.version)).append(" ( ").append(this.versionInt).append(" )").append("\n");
        if (this.flagShort != 0)
            stringBuilder.append(tabs).append("\tFLAG: ").append(Arrays.toString(this.flag)).append(" ( ").append(this.flagShort).append(" )").append("\n");

        stringBuilder.append(tabs).append("\tTX_IN_COUNT: ").append("\n");
        stringBuilder.append(this.txInCountVarInt.toString(tabsCount + 1));
        stringBuilder.append(tabs).append("\tTX_IN: ( total : ").append(this.txInList.size()).append(" )").append("\n");
        for (int i = 0; i < Math.min(5, this.txInList.size()); ++i)
            stringBuilder.append(this.txInList.get(i).toString(tabsCount + 1));

        stringBuilder.append(tabs).append("\tTX_OUT_COUNT: ").append("\n");
        stringBuilder.append(this.txOutCountVarInt.toString(tabsCount + 1));
        stringBuilder.append(tabs + "\tTX_OUT: ( total : " + this.txOutList.size() + " )").append("\n");
        for (int i = 0; i < Math.min(5, this.txOutList.size()); ++i)
            stringBuilder.append(this.txOutList.get(i).toString(tabsCount + 1));

        if (this.flagShort != 0) {
            stringBuilder.append(tabs).append("\tTX_WITNESSES: ").append("\n");
            stringBuilder.append(this.txWitnesses.toString(tabsCount + 1));
        }

        stringBuilder.append(tabs).append("\tLOCKTIME: ").append(Arrays.toString(this.locktime)).append(" ( ").append(this.locktimeInt).append(" )").append("\n");
        stringBuilder.append(tabs).append("[ END: Tx Payload ]").append("\n");

        return stringBuilder.toString();
    }
}
