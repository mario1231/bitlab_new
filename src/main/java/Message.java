import java.nio.ByteBuffer;

/*
    @class Message
    Generic class representing whole message.

    This class has got three simple tasks:
    - encapsulate MessageHeader header and T payload into one class
    - pack them into one ByteBuffer (for sending)
    - print whole message [header, payload]
 */
public class Message<T extends Payload> {
    private MessageHeader header;
    private T payload;

    public MessageHeader getHeader() { return this.header; }
    public T getPayload() { return this.payload; }

    public Message(MessageHeader header, T payload) {
        this.header = header;
        this.payload = payload;
    }

    public ByteBuffer pack() {
        return ByteBuffer.allocate(this.header.getPayload().capacity() + this.payload.getPayload().capacity())
                .put(this.header.getPayload())
                .put(this.payload.getPayload());
    }

    public void print() {
        System.out.println(this.toString());
    }

    @Override
    public String toString() {
        return this.header.toString(0) + this.payload.toString(0);
    }
}
