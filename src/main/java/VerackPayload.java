import java.nio.ByteBuffer;

public class VerackPayload extends Payload {
    //Variables used to unpack (empty payload)

    //Variables used to pack (empty payload)

    //Generate Test Case
    public static VerackPayload GenerateTest() {
        return VerackPayload.Generate();
    }

    public static VerackPayload Generate() {
        VerackPayload verackPayload = new VerackPayload();
        verackPayload.pack();
        return verackPayload;
    }

    public VerackPayload() { }
    public VerackPayload(ByteBuffer payload) {
        this.payload = payload;
        this.unpack();
    }

    @Override
    public ByteBuffer pack() {
        return this.payload = ByteBuffer.allocate(0).flip();
    }

    @Override
    public void unpack() {
        //Nothing to do here
    }

    @Override
    public void print(int tabsCount) {
        System.out.println(this.toString(tabsCount));
    }

    @Override
    public String toString(int tabsCount) {
        String tabs = "\t".repeat(Math.max(0, tabsCount));

        return (tabs + "[ Verack Payload ]\n") + (tabs + "[ END: Verack Payload ]\n");
    }
}
