import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

public class RejectPayload extends Payload {
    //Variables used to unpack
    VarString messageVarString;
    byte ccodeByte;
    VarString reasonVarString;
    String dataString;

    //Variables used to pack
    private byte[] message;
    private byte[] ccode;
    private byte[] reason;
    private byte[] data;

    //Getters
    public VarString getMessageVarString() { return this.messageVarString; }
    public byte getCcodeByte() { return this.ccodeByte; }
    public VarString getReasonVarString() { return this.reasonVarString; }
    public String getDataString() { return this.dataString; }

    //Generate Test Case
    public static RejectPayload GenerateTest() {
        return RejectPayload.Generate(
                VarString.Generate("Reject Payload Generated Message"),
                Constants.CCODES[0],
                VarString.Generate("Reject Payload Generated Reason"),
                "data_cL7iN8r1VAFrHkz4ZyP95qo5WBO"
        );
    }

    //Generate
    public static RejectPayload Generate(VarString message, byte ccode, VarString reason, String data) {
        RejectPayload rejectPayload = new RejectPayload();
        rejectPayload.messageVarString = message;
        rejectPayload.ccodeByte = ccode;
        rejectPayload.reasonVarString = reason;
        rejectPayload.dataString = data;

        rejectPayload.pack();
        return rejectPayload;
    }

    public RejectPayload() { }
    public RejectPayload (ByteBuffer payload) {
        this.payload = payload;
        this.unpack();
    }

    @Override
    public ByteBuffer pack() {
        //Pack into byte structures
        this.message = this.messageVarString.getPayload().array();
        this.ccode = new byte[] { this.ccodeByte };
        this.reason = this.reasonVarString.getPayload().array();
        this.data = Converter.stringToBytes(this.dataString, false).array();

        this.payload = ByteBuffer.allocate(this.message.length + this.ccode.length + this.reason.length + this.data.length)
                .order(ByteOrder.LITTLE_ENDIAN)
                .put(this.message).put(this.ccode).put(this.reason).put(this.data)
                .flip();

        return this.payload;
    }

    @Override
    public void unpack() {
        //MESSAGE
        int length = VarInt.unpackLength(this.payload.get());
            this.payload.position(this.payload.position() - 1);
            VarInt varInt = new VarInt(
                    ByteBuffer.allocate(length).order(ByteOrder.LITTLE_ENDIAN)
                            .put(Arrays.copyOfRange(this.payload.array(), this.payload.position(), this.payload.position() + length))
                    .flip()
            );
        int stringLength = (int)varInt.getValueLong();
        this.messageVarString = new VarString(
                ByteBuffer.allocate(stringLength + length).order(ByteOrder.LITTLE_ENDIAN)
                        .put(Arrays.copyOfRange(this.payload.array(), this.payload.position(), this.payload.position() + stringLength + length))
                .flip()
        );
        this.payload.position(this.payload.position() + stringLength + length);
        this.message = Converter.stringToBytes(messageVarString.getStringString(), false).array();

        //CCODE
        this.ccodeByte = this.payload.get();
        this.ccode = new byte[] { this.ccodeByte };

        //REASON
        length = VarInt.unpackLength(this.payload.get());
        this.payload.position(this.payload.position() - 1);
        varInt = new VarInt(
                ByteBuffer.allocate(length).order(ByteOrder.LITTLE_ENDIAN)
                        .put(Arrays.copyOfRange(this.payload.array(), this.payload.position(), this.payload.position() + length))
                .flip()
        );
        stringLength = (int)varInt.getValueLong();
        this.reasonVarString = new VarString(
                ByteBuffer.allocate(stringLength + length).order(ByteOrder.LITTLE_ENDIAN)
                        .put(Arrays.copyOfRange(this.payload.array(), this.payload.position(), this.payload.position() + stringLength + length))
                .flip()
        );
        this.payload.position(this.payload.position() + stringLength + length);
        this.reason = Converter.stringToBytes(reasonVarString.getStringString(), false).array();

        //DATA
        StringBuilder stringBuilder = new StringBuilder();
        while (this.payload.hasRemaining())
            stringBuilder.append((char)this.payload.get());

        this.dataString = stringBuilder.toString();
        this.data = Converter.stringToBytes(this.dataString, false).array();
    }

    @Override
    public void print(int tabsCount) {
        System.out.println(this.toString(tabsCount));
    }

    @Override
    public String toString(int tabsCount) {
        StringBuilder stringBuilder = new StringBuilder();
        String tabs = "\t".repeat(Math.max(0, tabsCount));

        stringBuilder.append(tabs).append("[ Reject Payload ]").append("\n");
        stringBuilder.append(tabs).append("\tMESSAGE:").append("\n");
        stringBuilder.append(this.messageVarString.toString(tabsCount + 1));
        stringBuilder.append(tabs).append("\tCCODE: ").append(Integer.toHexString(this.ccodeByte)).append("\n");
        stringBuilder.append(tabs).append("\tREASON:").append("\n");
        stringBuilder.append(this.reasonVarString.toString(tabsCount + 1));
        stringBuilder.append(tabs).append("\tDATA: ").append(this.dataString).append("\n");
        stringBuilder.append(tabs).append("[ END: Reject Payload ]").append("\n");

        return stringBuilder.toString();
    }
}
