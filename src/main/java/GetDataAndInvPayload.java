import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;

public class GetDataAndInvPayload extends Payload {
    //Variables used to unpack
    VarInt countVarInt;
    ArrayList<InvVect> invVectList;

    //Variables used to pack
    byte[] count;
    byte[] inventory;

    //Getters
    public VarInt getCountVarInt() { return this.countVarInt; }
    public ArrayList<InvVect> getInvVectList() { return this.invVectList; }

    //Generate Test
    public static GetDataAndInvPayload GenerateTest() {
        ArrayList<InvVect> list = new ArrayList<>();
        for (int i = 0; i < 5; ++i)
            list.add(InvVect.GenerateTest());
        return GetDataAndInvPayload.Generate(list);
    }

    //Generate
    public static GetDataAndInvPayload Generate(ArrayList<InvVect> vectors) {
        GetDataAndInvPayload getDataAndInvPayload = new GetDataAndInvPayload();

        ArrayList<InvVect> list = new ArrayList<>();

        for (int i = 0; i < Math.min(5, vectors.size()); ++i)
            list.add(vectors.get(i));

        getDataAndInvPayload.countVarInt = VarInt.Generate(list.size());
        getDataAndInvPayload.invVectList = list;

        getDataAndInvPayload.pack();
        return getDataAndInvPayload;
    }

    public GetDataAndInvPayload() { }
    public GetDataAndInvPayload(ByteBuffer payload) {
        this.payload = payload;
        this.unpack();
    }

    @Override
    public ByteBuffer pack() {
        //Helper Variables
        ByteBuffer invBuffer = ByteBuffer.allocate(36 * this.invVectList.size())
                .order(ByteOrder.LITTLE_ENDIAN);
        for (InvVect invVect : this.invVectList)
            invBuffer.put(invVect.getPayload());

        //Pack into byte structures
        this.count = this.countVarInt.getPayload().array();
        this.inventory = invBuffer.array();

        //Create Payload
        this.payload = ByteBuffer.allocate(this.count.length + this.inventory.length)
                .order(ByteOrder.LITTLE_ENDIAN)
                .put(this.count).put(this.inventory)
                .flip();

        return this.payload;
    }

    @Override
    public void unpack() {
        this.invVectList = new ArrayList<>();

        int length = VarInt.unpackLength(this.payload.get());
        this.payload.position(this.payload.position() - 1);
        this.countVarInt = new VarInt(
                ByteBuffer.allocate(length).order(ByteOrder.LITTLE_ENDIAN)
                        .put(Arrays.copyOfRange(this.payload.array(), this.payload.position(), this.payload.position() + length))
                        .flip()
        );
        this.payload.position(this.payload.position() + length);

        int count = (int) this.countVarInt.getValueLong();
        int startPosition = this.payload.position();
        for (int i = 0; i < count; ++i) {
            InvVect invVect = new InvVect(
                    ByteBuffer.allocate(36).order(ByteOrder.LITTLE_ENDIAN)
                            .put(Arrays.copyOfRange(this.payload.array(), startPosition + 36 * i, startPosition + 36 * (i+1)))
                            .flip()
            );
            this.invVectList.add(invVect);
        }
    }

    @Override
    public void print(int tabsCount) {
        System.out.println(this.toString(tabsCount));
    }

    @Override
    public String toString(int tabsCount) {
        StringBuilder stringBuilder = new StringBuilder();
        String tabs = "\t".repeat(Math.max(0, tabsCount));

        stringBuilder.append(tabs).append("[ GetData / Inv Payload ]").append("\n");
        stringBuilder.append(tabs).append("\tCOUNT:").append("\n");
        stringBuilder.append(this.countVarInt.toString(tabsCount + 1));
        stringBuilder.append(tabs).append("\tINVENTORY: ( total : ").append(this.invVectList.size()).append(" )").append("\n");
        for (int i = 0; i < Math.min(5, this.invVectList.size()); ++i)
            stringBuilder.append(this.invVectList.get(i).toString(tabsCount + 1));
        stringBuilder.append(tabs).append("\n").append("[ END: GetData / Inv Payload ]");

        return stringBuilder.toString();
    }
}
