import java.nio.ByteBuffer;

public class App {
    public static void main(String[] args) {
        Peer peer = new Peer();
        Menu menu = new Menu(peer);
        peer.setMenu(menu);

        menu.run();
    }
}
