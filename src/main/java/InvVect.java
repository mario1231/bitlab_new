import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.Random;

public class InvVect extends Payload {
    //Variables used to unpack
    int typeInt;
    String hashString;

    //Variables used to pack
    byte[] type;
    byte[] hash;

    //Getters
    public int getTypeInt() { return this.typeInt; }
    public String getHashString() { return this.hashString; }

    //Generate Test Case
    public static InvVect GenerateTest() {
        return InvVect.Generate(
            new Random().nextInt(),
            "IaAf6tmdddye6vYrcLvwbQDnnEn5A10F"
        );
    }

    //Generate
    public static InvVect Generate(int type, String hash) {
        InvVect invVect = new InvVect();
        invVect.typeInt = type;
        invVect.hashString = hash;

        invVect.pack();
        return invVect;
    }

    public InvVect() { }
    public InvVect(ByteBuffer payload) {
        this.payload = payload;
        this.unpack();
    }

    @Override
    public ByteBuffer pack() {
        //Pack into byte structures
        this.type = Converter.intToByteBuffer(this.typeInt).array();
        this.hash = Converter.stringToBytes(this.hashString, false).array();

        //Create Payload
        this.payload = ByteBuffer.allocate(this.type.length + this.hash.length)
                .order(ByteOrder.LITTLE_ENDIAN)
                .put(this.type).put(this.hash)
                .flip();

        return this.payload;
    }

    @Override
    public void unpack() {
        //Unpack into byte structures
        this.type = Arrays.copyOfRange(this.payload.array(), 0, 4);
        this.hash = Arrays.copyOfRange(this.payload.array(), 4, 36);

        //Init Variables with unpacked byte structures
        this.typeInt = Converter.bytesToUInt32(this.type);
        this.hashString = Converter.bytesToString(this.hash);
    }

    @Override
    public void print(int tabsCount) {
        System.out.println(this.toString(tabsCount));
    }

    @Override
    public String toString(int tabsCount) {
        StringBuilder stringBuilder = new StringBuilder();
        String tabs = "\t".repeat(Math.max(0, tabsCount));

        stringBuilder.append(tabs).append("[ InvVect ]").append("\n");
        stringBuilder.append(tabs).append("\tTYPE: ").append(Arrays.toString(this.type)).append(" ( ").append(this.typeInt).append(" )").append("\n");;
        stringBuilder.append(tabs).append("\tHASH: ").append(Arrays.toString(this.hash)).append(" ( ").append(this.hashString).append(" )").append("\n");;
        stringBuilder.append(tabs).append("[ END: InvVect ]").append("\n");;

        return stringBuilder.toString();
    }
}
