import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;

public class HeadersPayload extends Payload {
    //Variables used to unpack
    VarInt countVarInt;
    ArrayList<BlockHeader> blockHeadersList;

    //Variables used to pack
    byte[] count;
    byte[] headers;

    //Getters
    public VarInt getCountVarInt() { return this.countVarInt; }
    public ArrayList<BlockHeader> getBlockHeadersList() { return this.blockHeadersList; }

    //Generate Test Case
    public static HeadersPayload GenerateTest() {
        ArrayList<BlockHeader> list = new ArrayList<>();
        for (int i = 0; i < 5; ++i)
            list.add(BlockHeader.GenerateTest());
        return HeadersPayload.Generate(list);
    }

    //Generate
    public static HeadersPayload Generate(ArrayList<BlockHeader> list) {
        HeadersPayload headersPayload = new HeadersPayload();
        headersPayload.countVarInt = VarInt.Generate(list.size());
        headersPayload.blockHeadersList = list;

        headersPayload.pack();
        return headersPayload;
    }

    public HeadersPayload() { }
    public HeadersPayload(ByteBuffer payload) {
        this.payload = payload;
        this.unpack();
    }

    @Override
    public ByteBuffer pack() {
        //Helper Variables
        ByteBuffer byteBuffer;

        //COUNT
        this.count = countVarInt.getPayload().array();

        //HEADERS
        byteBuffer = ByteBuffer.allocate(this.blockHeadersList.size() * 81).order(ByteOrder.LITTLE_ENDIAN);
        for (BlockHeader blockHeader : this.blockHeadersList) {
            byteBuffer.put(blockHeader.getPayload().array());
        }

        this.headers = byteBuffer.array();

        this.payload = ByteBuffer.allocate(this.count.length + this.headers.length)
                .order(ByteOrder.LITTLE_ENDIAN)
                .put(this.count).put(this.headers)
                .flip();

        return this.payload;
    }

    @Override
    public void unpack() {
        //Help Variables
        ByteBuffer byteBuffer;
        byte[] bytes = new byte[81];

        //Init Variables
        this.blockHeadersList = new ArrayList<>();

        //COUNT
        int length = VarInt.unpackLength(this.payload.get());
        this.payload.position(this.payload.position() - 1);
        this.countVarInt = new VarInt(
                ByteBuffer.allocate(length).order(ByteOrder.LITTLE_ENDIAN)
                        .put(Arrays.copyOfRange(this.payload.array(), this.payload.position(), this.payload.position() + length))
                .flip()
        );
        this.payload.position(this.payload.position() + length);

        //HEADERS
        byteBuffer = ByteBuffer.allocate(81 * (int)this.countVarInt.getValueLong()).order(ByteOrder.LITTLE_ENDIAN);

        for (int i = 0; i < (int) this.countVarInt.getValueLong(); ++i) {
            this.payload.get(bytes);
            byteBuffer.put(bytes);
            this.blockHeadersList.add(new BlockHeader(
                    ByteBuffer.allocate(81).order(ByteOrder.LITTLE_ENDIAN).put(bytes).flip()
            ));
        }
    }

    @Override
    public void print(int tabsCount) {
        System.out.println(this.toString(tabsCount));
    }

    @Override
    public String toString(int tabsCount) {
        StringBuilder stringBuilder = new StringBuilder();
        String tabs = "\t".repeat(Math.max(0, tabsCount));

        stringBuilder.append(tabs).append("[ Headers Payload ]").append("\n");
        stringBuilder.append(tabs).append("\tCOUNT:").append("\n");
        stringBuilder.append(this.countVarInt.toString(tabsCount + 1));
        stringBuilder.append(tabs).append("\tHEADERS: ( total : ").append(this.blockHeadersList.size()).append(" )").append("\n");
        for (int i = 0; i < Math.min(5, this.blockHeadersList.size()); ++i)
            stringBuilder.append(this.blockHeadersList.get(i).toString(tabsCount + 1));
        stringBuilder.append(tabs).append("[ END: Headers Payload ]").append("\n");

        return stringBuilder.toString();
    }
}
