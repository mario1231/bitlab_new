import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.Random;

public class TxIn extends Payload {
    //Variables used to unpack
    private Outpoint previousOutputOutpoint;
    private VarInt scirptLengthVarInt;
    private String signatureScriptString;
    private int sequenceInt;

    //Variables used to pack
    private byte[] previous_output;
    private byte[] script_length;
    private byte[] signature_script;
    private byte[] sequence;

    //Getters
    public Outpoint getPreviousOutputOutpoint() { return this.previousOutputOutpoint; }
    public VarInt getScirptLengthVarInt() { return this.scirptLengthVarInt; }
    public String getSignatureScriptString() { return this.signatureScriptString; }
    public int getSequenceInt() { return this.sequenceInt; }

    //Generate Test Case
    public static TxIn GenerateTest() {
        return TxIn.GenerateTxIn(
                Outpoint.GenerateTest(),
                "script_uO933WL7iN8r1VAFrHkz4ZyP95",
                new Random().nextInt()
        );
    }

    //Generate
    public static TxIn GenerateTxIn(Outpoint outpoint, String signatureScript, int sequence) {
        TxIn txIn = new TxIn();
        txIn.previousOutputOutpoint = outpoint;
        txIn.scirptLengthVarInt = VarInt.Generate(signatureScript.length());
        txIn.signatureScriptString = signatureScript;
        txIn.sequenceInt = sequence;

        txIn.pack();
        return txIn;
    }

    public TxIn() { }
    public TxIn(ByteBuffer payload) {
        this.payload = payload;
        this.unpack();
    }

    @Override
    public ByteBuffer pack() {
        //Pack into byte structures
        this.previous_output = previousOutputOutpoint.getPayload().array();
        this.script_length = this.scirptLengthVarInt.getPayload().array();
        this.signature_script = Converter.stringToBytes(this.signatureScriptString, false).array();
        this.sequence = Converter.intToByteBuffer(this.sequenceInt).array();

        //Create Payload
        this.payload = ByteBuffer.allocate(this.previous_output.length + this.script_length.length + this.signature_script.length + this.sequence.length)
                .order(ByteOrder.LITTLE_ENDIAN)
                .put(this.previous_output).put(this.script_length).put(this.signature_script).put(this.sequence)
                .flip();

        return this.payload;
    }

    @Override
    public void unpack() {
        //PREVIOUS_OUTPUT
        this.previous_output = Arrays.copyOfRange(this.payload.array(), 0, 36);
        this.payload.position(this.payload.position() + 36);
        this.previousOutputOutpoint = new Outpoint(
            ByteBuffer.allocate(this.previous_output.length).order(ByteOrder.LITTLE_ENDIAN).put(this.previous_output).flip()
        );

        //SCRIPT_LENGTH
        int length = VarInt.unpackLength(this.payload.get());
        this.payload.position(this.payload.position() - 1);
        this.scirptLengthVarInt = new VarInt(
                ByteBuffer.allocate(length).order(ByteOrder.LITTLE_ENDIAN)
                        .put(Arrays.copyOfRange(this.payload.array(), this.payload.position(), this.payload.position() + length))
                .flip()
        );
        this.script_length = this.scirptLengthVarInt.pack().array();
        this.payload.position(this.payload.position() + length);

        //SIGNATURE_SCRIPT
        int size = (int) this.scirptLengthVarInt.getValueLong();
        this.signature_script = Arrays.copyOfRange(this.payload.array(), this.payload.position(), this.payload.position() + size);
        this.payload.position(this.payload.position() + size);
        this.signatureScriptString = Converter.bytesToString(this.signature_script);

        //SEQUENCE
        this.sequence = Converter.intToByteBuffer(this.payload.getInt()).array();
        this.sequenceInt = Converter.bytesToUInt32(this.sequence);
    }

    @Override
    public void print(int tabsCount) {
        System.out.println(this.toString(tabsCount));
    }

    @Override
    public String toString(int tabsCount) {
        StringBuilder stringBuilder = new StringBuilder();
        String tabs = "\t".repeat(Math.max(0, tabsCount));

        stringBuilder.append(tabs).append("[ TxIn ]").append("\n");
        stringBuilder.append(tabs).append("\tOUTPOINT: ").append("\n");
        stringBuilder.append(this.previousOutputOutpoint.toString(tabsCount + 1));
        stringBuilder.append(tabs).append("\tSCRIPT_LENGTH: ").append("\n");
        stringBuilder.append(this.scirptLengthVarInt.toString(tabsCount + 1));
        stringBuilder.append(tabs).append("\tSIGNATURE_SCRIPT: ").append(Arrays.toString(this.signature_script)).append(" ( ").append(this.signatureScriptString).append(" )").append("\n");
        stringBuilder.append(tabs).append("\tSEQUENCE: ").append(Arrays.toString(this.sequence)).append(" ( ").append(this.sequenceInt).append(" )").append("\n");
        stringBuilder.append(tabs).append("[ END: TxIn ]").append("\n");

        return stringBuilder.toString();
    }
}
