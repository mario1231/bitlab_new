import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Arrays;

public class Converter {
    public static int byteToUint8(byte b) {
        return b & 0xFF;
    }

    public static short bytesToUInt16(byte[] bytes) {
        return (short)((bytes[1] & 0xff) * 0x100 + (bytes[0] & 0xff));
    }

    public static short bytesToUInt16BigEndian(byte[] bytes) {
        return (short)((bytes[0] & 0xff) * 0x100 + (bytes[1] & 0xff));
    }

    public static Integer bytesToUInt32(byte[] bytes) {
        return (bytes[3] & 0xff) * 0x1000000 + (bytes[2] & 0xff) * 0x10000 + (bytes[1] & 0xff) * 0x100 + (bytes[0] & 0xff);
    }

    public static Integer bytesToUInt31BigEndian(byte[] bytes) {
        return (bytes[0] & 0xff) * 0x1000000 + (bytes[1] & 0xff) * 0x10000 + (bytes[2] & 0xff) * 0x100 + (bytes[3] & 0xff);
    }

    public static Long bytesToUInt64(byte[] bytes) {
        return (bytes[7] & 0xff) * 0x100000000000000L + (bytes[6] & 0xff) * 0x1000000000000L + (bytes[5] & 0xff) * 0x10000000000L + (bytes[4] & 0xff) * 0x100000000L + (bytes[3] & 0xff) * 0x1000000L + (bytes[2] & 0xff) * 0x10000L + (bytes[1] & 0xff) * 0x100L + (bytes[0] & 0xff);
    }

    public static ByteBuffer shortToByteBuffer(short s) {
        return ByteBuffer.allocate(2).order(ByteOrder.LITTLE_ENDIAN).putShort(s);
    }

    public static ByteBuffer shortToByteBufferBigEndian(short s) {
        return ByteBuffer.allocate(2).order(ByteOrder.BIG_ENDIAN).putShort(s);
    }

    public static ByteBuffer intToByteBuffer(int i) {
        return ByteBuffer.allocate(4).order(ByteOrder.LITTLE_ENDIAN).putInt(i);
    }

    public static ByteBuffer intToByteBufferBigEndian(int i) {
        return ByteBuffer.allocate(4).order(ByteOrder.BIG_ENDIAN).putInt(i);
    }

    public static ByteBuffer longToByteBuffer(long l) {
        return ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN).putLong(l);
    }

    public static ByteBuffer longToByteBufferBigEndian(long l) {
        return ByteBuffer.allocate(8).order(ByteOrder.BIG_ENDIAN).putLong(l);
    }

    public static boolean bytesToBoolean(byte[] bytes) {
        return bytes[0] != 0;
    }

    public static ByteBuffer stringAddressToBytes(String address) {
        byte[] bytes = address.getBytes();
        byte[] byteAddress = new byte[4];

        String[] split = address.split("\\.");
        byteAddress[0] = (byte)(Integer.parseInt(split[0]) & 0xFF);
        byteAddress[1] = (byte)(Integer.parseInt(split[1]) & 0xFF);
        byteAddress[2] = (byte)(Integer.parseInt(split[2]) & 0xFF);
        byteAddress[3] = (byte)(Integer.parseInt(split[3]) & 0xFF);

        return ByteBuffer.allocate(16)
                .put((byte)0).put((byte)0).put((byte)0).put((byte)0).put((byte)0).put((byte)0)
                .put((byte)0).put((byte)0).put((byte)0).put((byte)0).put((byte)0xff).put((byte)0xff)
                .put(byteAddress[0]).put(byteAddress[1]).put(byteAddress[2]).put(byteAddress[3]);
    }

    public static ByteBuffer stringToBytes(String s, boolean lengthForCommand) {
        int length = lengthForCommand ? Math.min(s.length(), 12) : s.length();
        byte[] bytes = new byte[length];

        for (int i = 0; i < length; ++i)
            bytes[i] = (byte) s.charAt(i);

        return ByteBuffer.allocate(lengthForCommand ? 12 : length).put(bytes);
    }

    public static String bytesToString(byte[] bytes) {
        StringBuilder sb = new StringBuilder();
        for (byte b : bytes) {
            if (b == (byte) 0)
                break;
            sb.append((char) b);
        }
        return sb.toString();
    }

    public static String byteAddressToString(byte[] bytes) {
        return "" + byteToUint8(bytes[12]) + "." + byteToUint8(bytes[13]) + "." + byteToUint8(bytes[14]) + "." + byteToUint8(bytes[15]);
    }

    public static String longTimestampToString(Long timestamp) {
        return new SimpleDateFormat("EEEEE dd MMMMM yyyy HH:mm:ss").format(new Timestamp(timestamp * 1000));
    }
}
