import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

public class PingPongPayload extends Payload {
    //Variables used to unpack
    long nonceLong;

    //Variables used to pack
    private byte[] nonce;

    //Getters
    public long getNonce() { return this.nonceLong; }

    //Generate Test Case
    public static PingPongPayload GenerateTest() {
        return PingPongPayload.Generate(Generator.generateNonce());
    }

    public static PingPongPayload Generate(long nonce) {
        PingPongPayload pingPongPayload = new PingPongPayload();
        pingPongPayload.nonceLong = nonce;

        pingPongPayload.pack();
        return pingPongPayload;
    }

    public PingPongPayload() { }
    public PingPongPayload(ByteBuffer payload) {
        this.payload = payload;
        this.unpack();
    }

    @Override
    public ByteBuffer pack() {
        //Pack into byte structures
        this.nonce = Converter.longToByteBuffer(this.nonceLong).array();

        //Create Payload
        this.payload = ByteBuffer.allocate(8).order(ByteOrder.LITTLE_ENDIAN)
                .put(this.nonce).flip();

        return this.payload;
    }

    @Override
    public void unpack() {
        //Unpack into byte structures
        this.nonce = Converter.longToByteBuffer(this.payload.getLong()).array();

        //Init Variables with unpacked byte structures
        this.nonceLong = Converter.bytesToUInt64(this.nonce);
    }

    @Override
    public void print(int tabsCount) {
        System.out.println(this.toString(tabsCount));
    }

    @Override
    public String toString(int tabsCount) {
        StringBuilder stringBuilder = new StringBuilder();
        String tabs = "\t".repeat(Math.max(0, tabsCount));

        stringBuilder.append(tabs).append("[ Ping/Pong Payload ]").append("\n");
        stringBuilder.append(tabs).append("\tNONCE: ").append(Arrays.toString(this.nonce)).append(" ( ").append(this.nonceLong).append(" )").append("\n");
        stringBuilder.append(tabs).append("[ END: Ping/Pong Payload ]").append("\n");

        return stringBuilder.toString();
    }
}
