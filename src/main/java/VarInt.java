import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.Random;

public class VarInt extends Payload {
    //Variables used to unpack
    private long valueLong;

    //Variables used to pack
    private byte[] value;

    //Helper Variables
    private byte lengthByte;
    private int payloadLength;

    //Getters
    public int getPayloadLength() { return this.payloadLength; }
    public long getValueLong() { return this.valueLong; }

    //Helper Functions
    public static int unpackLength(byte lengthByte) {
        int lengthInt = lengthByte & 0xFF;
        return (lengthInt < 0xFD) ? 1 : (lengthInt == 0xFD) ? 3 : (lengthInt == 0xFE) ? 5 : 9;
    }

    private void calculatePayloadLength() {
        this.payloadLength = (this.valueLong < 0xFDL) ? 1 : (this.valueLong <= 0xFFFFL) ? 3 : (this.valueLong <= 0xFFFFFFFFL) ? 5 : 9;
    }

    //Generate Test Case
    public static VarInt GenerateTest() {
        return VarInt.Generate(new Random().nextLong());
    }

    public static VarInt Generate(long value) {
        VarInt varInt = new VarInt();
        varInt.valueLong = value;
        varInt.calculatePayloadLength();

        varInt.pack();
        return varInt;
    }

    public VarInt() { }
    public VarInt(ByteBuffer payload) {
        this.payload = payload;
        this.unpack();
    }

    @Override
    public ByteBuffer pack() {
        this.payload = ByteBuffer.allocate(this.payloadLength).order(ByteOrder.LITTLE_ENDIAN);

        switch (this.payloadLength) {
            case 1:
                this.lengthByte = (byte) this.valueLong;
                this.value = null;
                break;
            case 3:
                this.lengthByte = (byte) 0xFD;
                this.value = Converter.shortToByteBuffer((short)this.valueLong).array();
                break;
            case 5:
                this.lengthByte = (byte) 0xFE;
                this.value = Converter.intToByteBuffer((int)this.valueLong).array();
                break;
            case 9:
                this.lengthByte = (byte) 0xFF;
                this.value = Converter.longToByteBuffer(this.valueLong).array();
                break;
        }

        this.payload.put(this.lengthByte);

        if (this.value != null)
            this.payload.put(this.value);

        return this.payload.flip();
    }

    @Override
    public void unpack() {
        this.lengthByte = this.payload.get();

        switch (this.lengthByte & 0xFF) {
            case 0xFD:
                this.payloadLength = 3;
                this.value = Converter.shortToByteBuffer(this.payload.getShort()).array();
                this.valueLong = Converter.bytesToUInt16(this.value);
                break;
            case 0xFE:
                this.payloadLength = 5;
                this.value = Converter.intToByteBuffer(this.payload.getInt()).array();
                this.valueLong = Converter.bytesToUInt32(this.value);
                break;
            case 0xFF:
                this.payloadLength = 9;
                this.value = Converter.longToByteBuffer(this.payload.getLong()).array();
                this.valueLong = Converter.bytesToUInt64(this.value);
                break;

            default:
                this.payloadLength = 1;
                this.valueLong = this.lengthByte & 0xFF;
                break;
        }
    }

    @Override
    public void print(int tabsCount) {
        System.out.println(this.toString(tabsCount));
    }

    @Override
    public String toString(int tabsCount) {
        StringBuilder stringBuilder = new StringBuilder();
        String tabs = "\t".repeat(Math.max(0, tabsCount));

        stringBuilder.append(tabs).append("[ VarInt ]").append("\n");
        stringBuilder.append(tabs).append("\tLENGTH: 0x").append(Integer.toHexString(this.lengthByte & 0xFF)).append(" ( ").append(this.payloadLength).append(" )").append("\n");

        switch (this.payloadLength) {
            case 1:
                stringBuilder.append(tabs).append("\tVALUE: ").append(Arrays.toString(this.value)).append(" ( ").append(Converter.byteToUint8(this.lengthByte)).append(" ) ").append("\n");
                break;
            case 3:
                stringBuilder.append(tabs).append("\tVALUE: ").append(Arrays.toString(this.value)).append(" ( ").append(Converter.bytesToUInt16(this.value)).append(" ) ").append("\n");
                break;
            case 5:
                stringBuilder.append(tabs).append("\tVALUE: ").append(Arrays.toString(this.value)).append(" ( ").append(Converter.bytesToUInt32(this.value)).append(" ) ").append("\n");
                break;
            case 9:
                stringBuilder.append(tabs).append("\tVALUE: ").append(Arrays.toString(this.value)).append(" ( ").append(Converter.bytesToUInt64(this.value)).append(" ) ").append("\n");
                break;
        }

        stringBuilder.append(tabs).append("[ END: VarInt ]").append("\n");

        return stringBuilder.toString();
    }
}
