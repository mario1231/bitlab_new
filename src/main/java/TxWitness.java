import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.Random;

public class TxWitness extends Payload {
    //Variables used to unpack
    VarInt lengthVarInt;
    String dataString;

    //Variables used to pack
    private byte[] length;
    private byte[] data;

    //Getters
    public VarInt getLengthVarInt() { return this.lengthVarInt; }
    public String getDataString() { return this.dataString; }

    //Generate Test Case
    public static TxWitness GenerateTest() {
        return TxWitness.Generate("Data_" + new Random().nextLong());
    }

    //Generate
    public static TxWitness Generate(String data) {
        TxWitness txWitness = new TxWitness();
        txWitness.dataString = data;
        txWitness.lengthVarInt = VarInt.Generate(txWitness.dataString.length());

        txWitness.pack();
        return txWitness;
    }

    public TxWitness() { }
    public TxWitness(ByteBuffer payload) {
        this.payload = payload;
        this.unpack();
    }

    @Override
    public ByteBuffer pack() {
        //Pack into byte structures
        this.length = this.lengthVarInt.pack().array();
        this.data = Converter.stringToBytes(this.dataString, false).array();

        //Create Payload
        this.payload = ByteBuffer.allocate(this.length.length + this.data.length)
                .order(ByteOrder.LITTLE_ENDIAN)
                .put(this.length).put(this.data)
                .flip();

        return this.payload;
    }

    @Override
    public void unpack() {
        //LENGTH
        int length = VarInt.unpackLength(this.payload.get());
        this.payload.position(this.payload.position() - 1);
        this.lengthVarInt = new VarInt(
                ByteBuffer.allocate(length).order(ByteOrder.LITTLE_ENDIAN)
                        .put(Arrays.copyOfRange(this.payload.array(), this.payload.position(), this.payload.position() + length))
                .flip()
        );
        this.length = this.lengthVarInt.getPayload().array();
        this.payload.position(this.payload.position() + length);

        //DATA
        int size = (int) this.lengthVarInt.getValueLong();
        this.data = Arrays.copyOfRange(this.payload.array(), this.payload.position(), this.payload.position() + size);
        this.dataString = Converter.bytesToString(this.data);
    }

    @Override
    public void print(int tabsCount) {
        System.out.println(this.toString(tabsCount));
    }

    @Override
    public String toString(int tabsCount) {
        StringBuilder stringBuilder = new StringBuilder();
        String tabs = "\t".repeat(Math.max(0, tabsCount));

        stringBuilder.append(tabs).append("[ TxWitness ]").append("\n");
        stringBuilder.append(tabs).append("\tLENGTH:").append("\n");
        stringBuilder.append(this.lengthVarInt.toString(tabsCount + 1)).append("\n");
        stringBuilder.append(tabs).append("\tDATA: ").append(Arrays.toString(this.data)).append(" ( ").append(this.dataString).append(" )").append("\n");
        stringBuilder.append(tabs).append("[ END: TxWitness ]").append("\n");

        return stringBuilder.toString();
    }
}
