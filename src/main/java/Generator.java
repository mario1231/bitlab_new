import java.nio.ByteBuffer;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Timestamp;
import java.util.Random;

public class Generator {
    public static long generateNonce() {
        return new Random().nextLong();
    }

    public static long generateTimestamp() {
        return new Timestamp(System.currentTimeMillis()).getTime() / 1000;
    }

    public static int generateChecksum(ByteBuffer payload) {
        try {
            MessageDigest digest = MessageDigest.getInstance("SHA-256");
            byte[] hash = digest.digest(digest.digest(payload.array()));

            return ByteBuffer.allocate(4)
                    .put(hash[3]).put(hash[2]).put(hash[1]).put(hash[0]).flip().getInt();
        } catch (NoSuchAlgorithmException ex) {
            return 0;
        }
    }
}