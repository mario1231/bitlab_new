import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Random;

public class BlockHeader extends Payload {
    //Variables used to unpack
    int versionInt;
    String prevBlockString;
    String merkleRootString;
    long timestampLong;
    int bitsInt;
    int nonceInt;
    VarInt txnCountVarInt;

    //Variables used to pack
    private byte[] version;
    private byte[] prev_block;
    private byte[] merkle_root;
    private byte[] timestamp;
    private byte[] bits;
    private byte[] nonce;
    private byte[] txn_count;

    //Getters
    public int getVersionInt() { return this.versionInt; }
    public String getPrevBlockString() { return this.prevBlockString; }
    public String getMerkleRootString() { return this.merkleRootString; }
    public long getTimestampLong() { return this.timestampLong; }
    public int getBitsInt() { return this.bitsInt; }
    public int getNonceInt() { return this.nonceInt; }
    public VarInt getTxnCountVarInt() { return this.txnCountVarInt; }

    //Generate Test Case
    public static BlockHeader GenerateTest() {
        return BlockHeader.Generate(
                Constants.VERSION,
                "GjZ9ft6wK1FpaV892ihnRq64MIS1kMOT",
                "uVhV9QLju6wSdyUILQuokNQjkkh89RoT",
                Generator.generateTimestamp(),
                new Random().nextInt(),
                (int) Generator.generateNonce(),
                VarInt.Generate(1)//VarInt.Generate(new Random().nextInt(10) + 5)
        );
    }

    //Generate
    public static BlockHeader Generate(int version, String prevBlock, String merkleRoot, long timestamp, int bits, int nonce, VarInt txnCount) {
        BlockHeader blockHeader = new BlockHeader();

        blockHeader.versionInt = version;
        blockHeader.prevBlockString = prevBlock;
        blockHeader.merkleRootString = merkleRoot;
        blockHeader.timestampLong = timestamp;
        blockHeader.bitsInt = bits;
        blockHeader.nonceInt = nonce;
        blockHeader.txnCountVarInt = txnCount;

        blockHeader.pack();
        return blockHeader;
    }

    public BlockHeader() { }
    public BlockHeader(ByteBuffer payload) {
        this.payload = payload;
        this.unpack();
    }

    @Override
    public ByteBuffer pack() {
        //Pack into byte structures
        this.version = Converter.intToByteBuffer(this.versionInt).array();
        this.prev_block = Converter.stringToBytes(this.prevBlockString, false).array();
        this.merkle_root = Converter.stringToBytes(this.merkleRootString, false).array();
        this.timestamp = Converter.intToByteBuffer((int) this.timestampLong).array();
        this.bits = Converter.intToByteBuffer(this.bitsInt).array();
        this.nonce = Converter.intToByteBuffer(this.nonceInt).array();
        this.txn_count = this.txnCountVarInt.getPayload().array();

        //Create payload;
        this.payload = ByteBuffer.allocate(
                this.version.length + this.prev_block.length + this.merkle_root.length +
                        this.timestamp.length + this.bits.length + this.nonce.length + this.txn_count.length
        ).order(ByteOrder.LITTLE_ENDIAN)
                .put(this.version).put(this.prev_block).put(this.merkle_root).put(this.timestamp)
                .put(this.bits).put(this.nonce).put(this.txn_count)
                .flip();

        return this.payload;
    }

    @Override
    public void unpack() {
        //Unpack into byte structures
        this.version = Converter.intToByteBuffer(this.payload.order(ByteOrder.LITTLE_ENDIAN).getInt()).array();
        this.prev_block = Arrays.copyOfRange(this.payload.array(), this.payload.position(), this.payload.position() + 32);
        this.payload.position(this.payload.position() + 32);
        this.merkle_root = Arrays.copyOfRange(this.payload.array(), this.payload.position(), this.payload.position() + 32);
        this.payload.position(this.payload.position() + 32);
        this.timestamp = Converter.intToByteBuffer(this.payload.order(ByteOrder.LITTLE_ENDIAN).getInt()).array();
        this.bits = Converter.intToByteBuffer(this.payload.order(ByteOrder.LITTLE_ENDIAN).getInt()).array();
        this.nonce = Converter.intToByteBuffer(this.payload.order(ByteOrder.LITTLE_ENDIAN).getInt()).array();

        //Init Variables with unpacked byte structures
        this.versionInt = Converter.bytesToUInt32(this.version);
        this.prevBlockString = Converter.bytesToString(this.prev_block);
        this.merkleRootString = Converter.bytesToString(this.merkle_root);
        this.timestampLong = Converter.bytesToUInt32(this.timestamp);
        this.bitsInt = Converter.bytesToUInt32(this.bits);
        this.nonceInt = Converter.bytesToUInt32(this.nonce);

        int length = VarInt.unpackLength(this.payload.get());
        this.payload.position(this.payload.position() - 1);
        this.txn_count = new byte[length];
        this.payload.get(txn_count);
        this.txnCountVarInt = new VarInt(
                ByteBuffer.allocate(txn_count.length).order(ByteOrder.LITTLE_ENDIAN)
                        .put(txn_count).flip()
        );
    }

    @Override
    public void print(int tabsCount) {
        System.out.println(this.toString(tabsCount));
    }

    @Override
    public String toString(int tabsCount) {
        StringBuilder stringBuilder = new StringBuilder();
        String tabs = "\t".repeat(Math.max(0, tabsCount));

        stringBuilder.append(tabs).append("[ BlockHeader Payload ]").append("\n");
        stringBuilder.append(tabs).append("\tVERSION: ").append(Arrays.toString(this.version)).append(" ( ").append(this.versionInt).append(" )").append("\n");
        stringBuilder.append(tabs).append("\tPREV_BLOCK: ").append(Arrays.toString(this.prev_block)).append(" ( ").append(this.prevBlockString).append(" )").append("\n");
        stringBuilder.append(tabs).append("\tMERKLE_ROOT: ").append(Arrays.toString(this.merkle_root)).append(" ( ").append(this.merkleRootString).append(" )").append("\n");
        stringBuilder.append(tabs).append("\tTIMESTAMP: ").append(Arrays.toString(this.timestamp)).append(" ( ").append(Converter.longTimestampToString(this.timestampLong)).append(" )").append("\n");
        stringBuilder.append(tabs).append("\tBITS: ").append(Arrays.toString(this.bits)).append(" ( ").append(this.bitsInt).append(" )").append("\n");
        stringBuilder.append(tabs).append("\tNONCE: ").append(Arrays.toString(this.nonce)).append(" ( ").append(this.nonceInt).append(" )").append("\n");
        stringBuilder.append(tabs).append("\tTXN_COUNT: ").append("\n");
        stringBuilder.append(this.txnCountVarInt.toString(tabsCount + 1));
        stringBuilder.append(tabs).append("[ END: BlockHeader Payload ]").append("\n");

        return stringBuilder.toString();
    }
}
