import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;

public class MessageHeader extends Payload {
    //Variables used to unpack
    int magicInt;
    String commandString;
    int lengthInt;
    int checksumInt;

    //Variables used to pack
    public byte[] magic;
    public byte[] command;
    public byte[] length;
    public byte[] checksum;

    //Getters
    public int getMagicInt() { return this.magicInt; }
    public String getCommandString() { return this.commandString; }
    public int getLengthInt() { return this.lengthInt; }
    public int getChecksumInt() { return this.checksumInt; }

    //Generate Test Case
    public static MessageHeader GenerateTest() {
        GetDataAndInvPayload g = GetDataAndInvPayload.GenerateTest();

        return MessageHeader.Generate(
                Constants.MAGIC,
                "version",
                g.getPayload().capacity(),
                Generator.generateChecksum(g.getPayload())
        );
    }

    //Generate
    public static MessageHeader Generate(int magic, String command, int length, int checksum) {
        MessageHeader messageHeader = new MessageHeader();
        messageHeader.magicInt = magic;
        messageHeader.commandString = command;
        messageHeader.lengthInt = length;
        messageHeader.checksumInt = checksum;

        messageHeader.pack();
        return messageHeader;
    }

    public MessageHeader() { }
    public MessageHeader(ByteBuffer header) {
        this.payload = header;
        this.unpack();
    }

    @Override
    public ByteBuffer pack() {
        //Pack into byte structures
        this.magic = Converter.intToByteBuffer(this.magicInt).array();
        this.command = Converter.stringToBytes(this.commandString, true).array();
        this.length = Converter.intToByteBuffer(this.lengthInt).array();
        this.checksum = Converter.intToByteBuffer(this.checksumInt).array();

        //Create Payload
        this.payload = ByteBuffer.allocate(24).order(ByteOrder.LITTLE_ENDIAN)
                .put(this.magic).put(this.command).put(this.length).put(this.checksum)
                .flip();

        return this.payload;
    }

    @Override
    public void unpack() {
        //Unpack into byte structures
        this.magic = Arrays.copyOfRange(payload.array(), 0, 4);
        this.command = Arrays.copyOfRange(payload.array(), 4, 16);
        this.length = Arrays.copyOfRange(payload.array(), 16, 20);
        this.checksum = Arrays.copyOfRange(payload.array(), 20, 24);

        //Init Variables with unpacked byte structures
        this.magicInt = Converter.bytesToUInt32(this.magic);
        this.commandString = Converter.bytesToString(this.command);
        this.lengthInt = Converter.bytesToUInt32(this.length);
        this.checksumInt = Converter.bytesToUInt32(this.checksum);
    }

    @Override
    public void print(int tabsCount) {
        System.out.println(this.toString(tabsCount));
    }

    @Override
    public String toString(int tabsCount) {
        StringBuilder stringBuilder = new StringBuilder();
        String tabs = "\t".repeat(Math.max(0, tabsCount));

        stringBuilder.append(tabs).append("[ Header ]").append("\n");
        stringBuilder.append(tabs).append("\tMAGIC: ").append(Arrays.toString(this.magic)).append(" ( ").append(Integer.toHexString(this.magicInt)).append(" )").append("\n");
        stringBuilder.append(tabs).append("\tCOMMAND: ").append(Arrays.toString(this.command)).append(" ( ").append(this.commandString).append(" )").append("\n");
        stringBuilder.append(tabs).append("\tLENGTH: ").append(Arrays.toString(this.length)).append(" ( ").append(this.lengthInt).append(" )").append("\n");
        stringBuilder.append(tabs).append("\tCHECKSUM: ").append(Arrays.toString(this.checksum)).append(" ( ").append(this.checksumInt).append(" )").append("\n");
        stringBuilder.append(tabs).append("[ END: Header ]").append("\n");

        return stringBuilder.toString();
    }
}
