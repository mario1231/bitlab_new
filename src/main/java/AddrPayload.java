import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.ArrayList;

public class AddrPayload extends Payload {
    //Variables used to unpack
    private VarInt countVarInt;
    private ArrayList<NetAddrWithTime> addressesList;

    //Variables used to pack
    byte[] count;
    byte[] addr_list;

    //Getters
    public VarInt getCountVarInt() { return this.countVarInt; }
    public ArrayList<NetAddrWithTime> getAddressesList() { return this.addressesList; }

    //Generate Test Case
    public static AddrPayload GenerateTest() {
        ArrayList<NetAddrWithTime> addresses = new ArrayList<>();
        for (int i = 0; i++ < 5;)
            addresses.add(NetAddrWithTime.GenerateTest());
        AddrPayload addrPayload = AddrPayload.Generate(addresses);
        return addrPayload;
    }

    //Generate
    public static AddrPayload Generate(ArrayList<NetAddrWithTime> addresses) {
        AddrPayload addrPayload = new AddrPayload();
        addrPayload.countVarInt = VarInt.Generate(addresses.size());
        addrPayload.addressesList = addresses;

        addrPayload.pack();
        return addrPayload;
    }

    public AddrPayload() { }
    public AddrPayload(ByteBuffer payload) {
        this.payload = payload;
        this.unpack();
    }

    @Override
    public ByteBuffer pack() {
        //Help Variables
        ByteBuffer netByteBuffer = ByteBuffer.allocate(this.addressesList.size() * 30).order(ByteOrder.LITTLE_ENDIAN);
        for(NetAddrWithTime n : this.addressesList)
            netByteBuffer.put(n.pack());

        //Pack into byte structures
        this.count = this.countVarInt.pack().array();
        this.addr_list = netByteBuffer.array();

        //Create Payload
        this.payload = ByteBuffer.allocate(this.count.length + this.addr_list.length)
                .order(ByteOrder.LITTLE_ENDIAN)
                .put(this.count).put(this.addr_list)
                .flip();

        return this.payload;
    }

    @Override
    public void unpack() {
        //Help Variables
        int varIntLength = VarInt.unpackLength(this.payload.get());
        this.payload.position(this.payload.position() - 1);

        ByteBuffer addressesByteBuffer;

        //Init Variables
        this.addressesList = new ArrayList<>();

        //Unpack into byte and Variable structures
        this.count = new byte[varIntLength];
        this.payload.get(count);
        this.countVarInt = new VarInt(ByteBuffer.allocate(this.count.length).order(ByteOrder.LITTLE_ENDIAN).put(this.count).flip());

        addressesByteBuffer = ByteBuffer.allocate(30 * (int) countVarInt.getValueLong()).order(ByteOrder.LITTLE_ENDIAN);
        for (int i = 0; i < (int) countVarInt.getValueLong(); ++i) {
            byte[] fooByte = new byte[30];
            this.payload.get(fooByte);
            addressesByteBuffer.put(fooByte);

            this.addressesList.add(new NetAddrWithTime(
                    ByteBuffer.allocate(30).order(ByteOrder.LITTLE_ENDIAN).put(fooByte).flip()
            ));
        }
        this.addr_list = addressesByteBuffer.array();
    }

    @Override
    public void print(int tabsCount) {
        System.out.println(this.toString(tabsCount));
    }

    @Override
    public String toString(int tabsCount) {
        StringBuilder stringBuilder = new StringBuilder();
        String tabs = "\t".repeat(Math.max(0, tabsCount));

        stringBuilder.append(tabs).append("[ Addr Payload ]").append("\n");;
        stringBuilder.append(tabs).append("\tCOUNT:").append("\n");;
        stringBuilder.append(this.countVarInt.toString(tabsCount + 1));
        stringBuilder.append(tabs).append("\tADDR_LIST: ( total : ").append(this.addressesList.size()).append(" )").append("\n");;

        for (int i = 0; i < Math.min(5, this.addressesList.size()); ++i)
            stringBuilder.append(this.addressesList.get(i).toString(tabsCount + 1));

        stringBuilder.append(tabs).append("[ END: Addr Payload ]").append("\n");;

        return stringBuilder.toString();
    }
}
