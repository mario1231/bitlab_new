import java.nio.ByteBuffer;

public class GetAddrPayload extends Payload {
    //Variables used to unpack (none: empty payload)

    //Variables used to pack (only payload here)

    //Generate Test Case
    public static GetAddrPayload GenerateTest() {
        return GetAddrPayload.Generate();
    }

    //Generate
    public static GetAddrPayload Generate() {
        GetAddrPayload getAddrPayload = new GetAddrPayload();
        getAddrPayload.pack();
        return getAddrPayload;
    }

    public GetAddrPayload() { }
    public GetAddrPayload(ByteBuffer payload) {
        this.payload = payload;
        this.unpack();
    }

    @Override
    public ByteBuffer pack() {
        //Nothing to do here (empty payload)

        this.payload = ByteBuffer.allocate(0);
        return this.payload;
    }

    @Override
    public void unpack() {
        //Nothing to do here (empty payload)
    }

    @Override
    public void print(int tabsCount) {
        System.out.println(this.toString(tabsCount));
    }

    @Override
    public String toString(int tabsCount) {
        StringBuilder stringBuilder = new StringBuilder();
        String tabs = "\t".repeat(Math.max(0, tabsCount));

        stringBuilder.append(tabs).append("[ GetAddr Payload ]").append("\n");
        stringBuilder.append(tabs).append("[ END: GetAddr Payload ]").append("\n");

        return stringBuilder.toString();
    }
}
