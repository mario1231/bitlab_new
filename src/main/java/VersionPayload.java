import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.util.Arrays;
import java.util.Random;

public class VersionPayload extends Payload {
    //Variables used to unpack
    int versionInt;
    long servicesLong;
    long timestampLong;
    private NetAddr addrRecNetAddr;
    private NetAddr addrFromNetAddr;
    long nonceLong;
    private VarString userAgentVarStr;
    int startHeightInt;
    boolean relayBool;

    //Variables used to pack
    private byte[] version;
    private byte[] services;
    private byte[] timestamp;
    private byte[] addr_recv;
    private byte[] addr_from;
    private byte[] nonce;
    private byte[] user_agent;
    private byte[] start_height;
    private byte[] relay;

    //Getters
    public int getVersionInt() { return this.versionInt; }
    public long getServicesLong() { return this.servicesLong; }
    public long getTimestampLong() { return this.timestampLong; }
    public NetAddr getAddrRecNetAddr() { return this.addrRecNetAddr; }
    public NetAddr getAddrFromNetAddr() { return this.addrFromNetAddr; }
    public long getNonceLong() { return this.nonceLong; }
    public VarString getUserAgentVarStr() { return this.userAgentVarStr; }
    public int getStartHeightInt() { return this.startHeightInt; }
    public boolean getRelayBool() { return this.relayBool; }

    //Generate Test Case
    public static VersionPayload GenerateTest() {
        return VersionPayload.Generate(
            Constants.VERSION,
            new Random().nextLong(),
            Generator.generateTimestamp(),
            NetAddr.GenerateTest(),
            NetAddr.GenerateTest(),
            Generator.generateNonce(),
            VarString.Generate("User_agent_bitlab-2.0"),
            new Random().nextInt(),
            false
        );
    }

    //Generate
    public static VersionPayload Generate(int version, long services, long timestamp, NetAddr addressRecv, NetAddr addressFrom, long nonce, VarString userAgent, int startHeight, boolean relay) {
        VersionPayload versionPayload = new VersionPayload();
        versionPayload.versionInt = version;
        versionPayload.servicesLong = services;
        versionPayload.timestampLong = timestamp;
        versionPayload.addrRecNetAddr = addressRecv;
        versionPayload.addrFromNetAddr = addressFrom;
        versionPayload.nonceLong = nonce;
        versionPayload.userAgentVarStr = userAgent;
        versionPayload.startHeightInt = startHeight;
        versionPayload.relayBool = relay;

        versionPayload.pack();
        return versionPayload;
    }

    public VersionPayload() { }
    public VersionPayload(ByteBuffer payload) {
        this.payload = payload;
        this.unpack();
    }

    @Override
    public ByteBuffer pack() {
        //Pack into byte structures
        this.version = Converter.intToByteBuffer(this.versionInt).array();
        this.services = Converter.longToByteBuffer(this.servicesLong).array();
        this.timestamp = Converter.longToByteBuffer(this.timestampLong).array();
        this.addr_recv = this.addrRecNetAddr.getPayload().array();
        this.addr_from = this.addrFromNetAddr.getPayload().array();
        this.nonce = Converter.longToByteBuffer(this.nonceLong).array();
        this.user_agent = this.userAgentVarStr.getPayload().array();
        this.start_height = Converter.intToByteBuffer(this.startHeightInt).array();
        this.relay = new byte[] {(byte)(this.relayBool ? 1 : 0)};

        //Create Payload
        this.payload = ByteBuffer.allocate(85 + this.user_agent.length)
                .put(this.version).put(this.services)
                .put(this.timestamp).put(this.addr_recv).put(this.addr_from)
                .put(nonce).put(user_agent).put(start_height).put(relay)
                .flip();

        return this.payload;
    }

    @Override
    public void unpack() {
        //VERSION
        this.version = Converter.intToByteBuffer(this.payload.order(ByteOrder.LITTLE_ENDIAN).getInt()).array();
        this.versionInt = Converter.bytesToUInt32(this.version);

        //SERVICES
        this.services = Converter.longToByteBuffer(this.payload.order(ByteOrder.LITTLE_ENDIAN).getLong()).array();
        this.servicesLong = Converter.bytesToUInt64(this.services);

        //TIMESTAMP
        this.timestamp = Converter.longToByteBuffer(this.payload.order(ByteOrder.LITTLE_ENDIAN).getLong()).array();
        this.timestampLong = Converter.bytesToUInt64(this.timestamp);

        //ADDR_RECV
        this.addr_recv = Arrays.copyOfRange(this.payload.array(), this.payload.position(), this.payload.position() + 26);
        this.payload.position(this.payload.position() + 26);
        this.addrRecNetAddr = new NetAddr(ByteBuffer.allocate(26).put(this.addr_recv).order(ByteOrder.LITTLE_ENDIAN).flip());

        //ADDR_FROM
        this.addr_from = Arrays.copyOfRange(this.payload.array(), this.payload.position(), this.payload.position() + 26);
        this.payload.position(this.payload.position() + 26);
        this.addrFromNetAddr = new NetAddr(ByteBuffer.allocate(26).put(this.addr_from).order(ByteOrder.LITTLE_ENDIAN).flip());

        //NONCE
        this.nonce = Converter.longToByteBuffer(this.payload.getLong()).order(ByteOrder.LITTLE_ENDIAN).array();
        this.nonceLong = Converter.bytesToUInt64(this.nonce);

        //USER_AGENT
        int length = VarInt.unpackLength(this.payload.get());
        this.payload.position(this.payload.position() - 1);
        VarInt varInt = new VarInt(
                ByteBuffer.allocate(length).order(ByteOrder.LITTLE_ENDIAN)
                .put(Arrays.copyOfRange(this.payload.array(), this.payload.position(), this.payload.position() + length))
                .flip()
        );

        int stringLength = (int)varInt.getValueLong();
        this.userAgentVarStr = new VarString(
            ByteBuffer.allocate(stringLength + length).order(ByteOrder.LITTLE_ENDIAN)
                    .put(Arrays.copyOfRange(this.payload.array(), this.payload.position(), this.payload.position() + stringLength + length))
                .flip()
        );
        this.payload.position(this.payload.position() + stringLength + length);
        this.user_agent = Converter.stringToBytes(userAgentVarStr.getStringString(), false).array();

        ///START_HEIGHT
        this.start_height = Converter.intToByteBuffer(this.payload.getInt()).order(ByteOrder.LITTLE_ENDIAN).array();
        this.startHeightInt = Converter.bytesToUInt32(this.start_height);

        //RELAY
        this.relay = new byte[] { this.payload.get() };
        this.relayBool = this.relay[0] != 0;
    }

    @Override
    public void print(int tabsCount) {
        System.out.println(this.toString(tabsCount));
    }

    @Override
    public String toString(int tabsCount) {
        StringBuilder stringBuilder = new StringBuilder();
        String tabs = "\t".repeat(Math.max(0, tabsCount));

        stringBuilder.append(tabs).append("[ Version Payload ]").append("\n");
        stringBuilder.append(tabs).append("\tVERSION: ").append(Arrays.toString(this.version)).append(" ( ").append(this.versionInt).append(" )").append("\n");
        stringBuilder.append(tabs).append("\tSERVICES: ").append(Arrays.toString(this.services)).append(" ( ").append(this.servicesLong).append(" )").append("\n");
        stringBuilder.append(tabs).append("\tTIMESTAMP: ").append(Arrays.toString(this.timestamp)).append(" ( ").append(Converter.longTimestampToString(this.timestampLong)).append(" )").append("\n");
        stringBuilder.append(tabs).append("\tADDR_RECV: ").append("\n");
        stringBuilder.append(this.addrRecNetAddr.toString(tabsCount + 1));
        stringBuilder.append(tabs).append("\tADDR_FROM: ").append("\n");
        stringBuilder.append(this.addrFromNetAddr.toString(tabsCount + 1));
        stringBuilder.append(tabs).append("\tNONCE: ").append(Arrays.toString(this.nonce)).append(" ( ").append(this.nonceLong).append(" )").append("\n");
        stringBuilder.append(tabs).append("\tUSER_AGENT: ").append("\n");
        stringBuilder.append(this.userAgentVarStr.toString(tabsCount + 1));
        stringBuilder.append(tabs).append("\tSTART_HEIGHT: ").append(Arrays.toString(this.start_height)).append(" ( ").append(this.startHeightInt).append(" )").append("\n");
        stringBuilder.append(tabs).append("\tRELAY: ").append(Arrays.toString(this.relay)).append(" ( ").append(this.relayBool).append(" )").append("\n");
        stringBuilder.append(tabs).append("[ END: Version Payload ]").append("\n");

        return stringBuilder.toString();
    }
}
