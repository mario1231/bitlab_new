import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.*;
import java.util.stream.Collectors;

public class Peer implements Runnable {
    private Menu menu;

    public void setMenu(Menu menu) { this.menu = menu; }

    // Data structures for handling messages
    /**
     * List of last received nonces from ping
     */
    private HashMap<String, Long> pingNonces = new HashMap<>();
    /**
     * Set of Inventory Vectors
     */
    private ArrayList<InvVect> inventoryVectors = new ArrayList<>();
    /**
     * List of Addressess
     */
    private ArrayList<NetAddrWithTime> addresses = new ArrayList<>();
    /**
     * List of hashes
     */
    private Set<String> hashes = new HashSet<>();
    /**
     * Set of Blocks
     */
    private Set<BlockPayload> blocks = new HashSet<>();
    /**
     * Set of Transactions
     */
    private Set<TxPayload> transactions = new HashSet<>();

    /**
     * Am I on exit?
     */
    boolean onExit = false;
    /**
     * IP of local peer
     */
    private String myIP;

    /**
     * List of available IPs to connect
     */
    private ArrayList<String> availablePeers = new ArrayList<>();
    /**
     * List of connected peers
     */
    private ArrayList<String> connectedPeers = new ArrayList<>();
    /**
     * Selector for SocketChannels
     */
    private Selector selector = null;
    /**
     * Selection Keys
     */
    private ArrayList<SelectionKey> selectionKeys = new ArrayList<>();
    /**
     * List of Socket Channels of Peers
     */
    private ArrayList<SocketChannel> socketChannels = new ArrayList<>();

    /**
     * List of unread messages
     */
    private ArrayList<String> unreadMessages = new ArrayList<>();

    public Peer() {
        try {
            //Open Selector
            this.selector = Selector.open();
            //Find my IP on internet
            URL whatismyip = new URL("http://checkip.amazonaws.com");
            BufferedReader in = new BufferedReader(new InputStreamReader( whatismyip.openStream()));
            myIP = in.readLine();

            this.receiveIPs();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void sendMessage(String command, String[] params) {
        boolean displayOnly = (params != null && params[0].equals("-n"));

        try {
            for (SocketChannel socketChannel : this.socketChannels) {
                String socketRemoteAddress = socketChannel.getRemoteAddress().toString();
                socketRemoteAddress = socketRemoteAddress.substring(1, socketRemoteAddress.indexOf(":"));

                switch (command) {
                    case "version": {
                        System.out.println("{{ " + (displayOnly ? "VIEWING" : "SENDING") + " VERSION MESSAGE }}");
                        //Generate Payload
                        VersionPayload versionPayload = VersionPayload.Generate(
                                Constants.VERSION,
                                Constants.SERVICES,
                                Generator.generateTimestamp(),
                                NetAddr.Generate(Constants.SERVICES, socketRemoteAddress, (short) 8333),
                                NetAddr.Generate(Constants.SERVICES, myIP, (short) 8333),
                                Generator.generateNonce(),
                                VarString.Generate(Constants.USER_AGENT),
                                0,
                                false
                        );
                        //Generate Header
                        MessageHeader messageHeader = MessageHeader.Generate(
                                Constants.MAGIC,
                                "version",
                                versionPayload.getPayload().capacity(),
                                Generator.generateChecksum(versionPayload.getPayload())
                        );
                        //Generate Message {Header, Payload}
                        Message<VersionPayload> message = new Message<>(messageHeader, versionPayload);
                        //Show generated Message
                        message.print();
                        //Sent via SocketChannel
                        if (!displayOnly)
                            this.sendMessageToPeer(message, socketChannel);
                        System.out.println("{{ END: " + (displayOnly ? "VIEWING" : "SENDING") + " VERSION MESSAGE }}");
                    } break;

                    case "verack" : {
                        System.out.println("{{ " + (displayOnly ? "VIEWING" : "SENDING") + " VERACK MESSAGE }}");
                        VerackPayload verackPayload = VerackPayload.Generate();
                        MessageHeader messageHeader =  MessageHeader.Generate(
                                Constants.MAGIC,
                                "verack",
                                verackPayload.getPayload().capacity(),
                                Generator.generateChecksum(verackPayload.getPayload())
                        );
                        Message<VerackPayload> sendMessage = new Message<>(messageHeader, verackPayload);
                        sendMessage.print();
                        if (!displayOnly)
                            this.sendMessageToPeer(sendMessage, socketChannel);
                        System.out.println("{{ END: " + (displayOnly ? "VIEWING" : "SENDING") + " VERACK MESSAGE }}");
                    } break;

                    case "ping" : {
                        System.out.println("{{ " + (displayOnly ? "VIEWING" : "SENDING") + " PING MESSAGE }}");
                        PingPongPayload pingPongPayload = PingPongPayload.GenerateTest();
                        MessageHeader messageHeader =  MessageHeader.Generate(
                                Constants.MAGIC,
                                "ping",
                                pingPongPayload.getPayload().capacity(),
                                Generator.generateChecksum(pingPongPayload.getPayload())
                        );
                        Message<PingPongPayload> sendMessage = new Message<>(messageHeader, pingPongPayload);
                        sendMessage.print();
                        if (!displayOnly)
                            this.sendMessageToPeer(sendMessage, socketChannel);
                        System.out.println("{{ END: " + (displayOnly ? "VIEWING" : "SENDING") + " PING MESSAGE }}");
                    } break;

                    case "pong" : {
                        System.out.println("{{ " + (displayOnly ? "VIEWING" : "SENDING") + " PONG MESSAGE }}");
                        //Generate Payload
                        PingPongPayload pingPongPayload = PingPongPayload.Generate(this.pingNonces.get(socketRemoteAddress));
                        //Generate Header
                        MessageHeader messageHeader = MessageHeader.Generate(
                                Constants.MAGIC,
                                "pong",
                                pingPongPayload.getPayload().capacity(),
                                Generator.generateChecksum(pingPongPayload.getPayload())
                        );
                        //Generate Message {Header, Payload}
                        Message<PingPongPayload> sendMessage = new Message<>(messageHeader, pingPongPayload);
                        //Show generated Message
                        sendMessage.print();
                        //Sent via SocketChannel
                        if (!displayOnly)
                            this.sendMessageToPeer(sendMessage, socketChannel);
                        System.out.println("{{ END: " + (displayOnly ? "VIEWING" : "SENDING") + " PONG MESSAGE }}");
                    } break;

                    case "getaddr" : {
                        System.out.println("{{ " + (displayOnly ? "VIEWING" : "SENDING") + " GET_ADDR MESSAGE }}");
                        //Generate Payload
                        GetAddrPayload getAddrPayload = GetAddrPayload.Generate();
                        //Generate Header
                        MessageHeader messageHeader = MessageHeader.Generate(
                                Constants.MAGIC,
                                "getaddr",
                                getAddrPayload.getPayload().capacity(),
                                Generator.generateChecksum(getAddrPayload.getPayload())
                        );
                        //Generate Message {Header, Payload}
                        Message<GetAddrPayload> sendMessage = new Message<>(messageHeader, getAddrPayload);
                        //Show generated Message
                        sendMessage.print();
                        //Sent via SocketChannel
                        if (!displayOnly)
                            this.sendMessageToPeer(sendMessage, socketChannel);
                        System.out.println("{{ END: " + (displayOnly ? "VIEWING" : "SENDING") + " GET_ADDR MESSAGE }}");
                    } break;

                    case "addr" : {
                        System.out.println("{{ " + (displayOnly ? "VIEWING" : "SENDING") + " ADDR MESSAGE }}");
                        //Generate Payload
                        AddrPayload addrPayload = AddrPayload.Generate(this.addresses);
                        //Generate Header
                        MessageHeader messageHeader = MessageHeader.Generate(
                                Constants.MAGIC,
                                "addr",
                                addrPayload.getPayload().capacity(),
                                Generator.generateChecksum(addrPayload.getPayload())
                        );
                        //Generate Message {Header, Payload}
                        Message<AddrPayload> sendMessage = new Message<>(messageHeader, addrPayload);
                        //Show generated Message
                        sendMessage.print();
                        //Sent via SocketChannel
                        if (!displayOnly)
                            this.sendMessageToPeer(sendMessage, socketChannel);
                        System.out.println("{{ END: " + (displayOnly ? "VIEWING" : "SENDING") + " ADDR MESSAGE }}");
                    } break;

                    case "getdata" : {
                        System.out.println("{{ " + (displayOnly ? "VIEWING" : "SENDING") + " GET_DATA MESSAGE }}");
                        //Generate Payload
                        GetDataAndInvPayload getDataAndInvPayload = GetDataAndInvPayload.Generate(this.inventoryVectors);
                        //Generate Header
                        MessageHeader messageHeader = MessageHeader.Generate(
                                Constants.MAGIC,
                                "getdata",
                                getDataAndInvPayload.getPayload().capacity(),
                                Generator.generateChecksum(getDataAndInvPayload.getPayload())
                        );
                        //Generate Message {Header, Payload}
                        Message<GetDataAndInvPayload> sendMessage = new Message<>(messageHeader, getDataAndInvPayload);
                        //Show generated Message
                        sendMessage.print();
                        //Sent via SocketChannel
                        if (!displayOnly)
                            this.sendMessageToPeer(sendMessage, socketChannel);
                        System.out.println("{{ END: " + (displayOnly ? "VIEWING" : "SENDING") + " GET_DATA MESSAGE }}");
                    } break;

                    case "reject" : {
                        System.out.println("{{ " + (displayOnly ? "VIEWING" : "SENDING") + " REJECT MESSAGE }}");
                        //Generate Payload
                        RejectPayload rejectPayload = RejectPayload.GenerateTest();
                        //Generate Header
                        MessageHeader messageHeader = MessageHeader.Generate(
                                Constants.MAGIC,
                                "reject",
                                rejectPayload.getPayload().capacity(),
                                Generator.generateChecksum(rejectPayload.getPayload())
                        );
                        //Generate Message {Header, Payload}
                        Message<RejectPayload> sendMessage = new Message<>(messageHeader, rejectPayload);
                        //Show generated Message
                        sendMessage.print();
                        //Sent via SocketChannel
                        if (!displayOnly)
                            this.sendMessageToPeer(sendMessage, socketChannel);
                        System.out.println("{{ END: " + (displayOnly ? "VIEWING" : "SENDING") + " REJECT MESSAGE }}");
                    } break;

                    case "getheaders" : {
                        System.out.println("{{ " + (displayOnly ? "VIEWING" : "SENDING") + " GET_HEADERS MESSAGE }}");

                        if (this.inventoryVectors.size() > 0) {
                            ArrayList<String> list = new ArrayList<>();
                            list.add(this.inventoryVectors.get(new Random().nextInt(this.inventoryVectors.size())).hashString);

                            //Generate Payload
                            GetHeadersAndGetBlocksPayload getHeadersAndGetBlocksPayload = GetHeadersAndGetBlocksPayload.Generate(
                                    Constants.VERSION,
                                    list,
                                    "\0".repeat(32)
                            );
                            //Generate Header
                            MessageHeader messageHeader = MessageHeader.Generate(
                                    Constants.MAGIC,
                                    "getheaders",
                                    getHeadersAndGetBlocksPayload.getPayload().capacity(),
                                    Generator.generateChecksum(getHeadersAndGetBlocksPayload.getPayload())
                            );
                            //Generate Message {Header, Payload}
                            Message<GetHeadersAndGetBlocksPayload> sendMessage = new Message<>(messageHeader, getHeadersAndGetBlocksPayload);
                            //Show generated Message
                            sendMessage.print();
                            //Sent via SocketChannel
                            if (!displayOnly) {
                                this.sendMessageToPeer(sendMessage, socketChannel);
                            }
                        }
                        else {
                            System.out.println("ERROR: NO HASHES FOUND.");
                        }
                        System.out.println("{{ END: " + (displayOnly ? "VIEWING" : "SENDING") + " GET_HEADERS MESSAGE }}");
                    } break;

                    case "headers" : {
                        System.out.println("{{ " + (displayOnly ? "VIEWING" : "SENDING") + " HEADERS MESSAGE }}");
                        ArrayList<BlockHeader> list = new ArrayList<>();
                        for (BlockPayload block : this.blocks)
                            list.add(block.getBlockHeader());

                        //Generate Payload
                        HeadersPayload headersPayload = HeadersPayload.Generate(list);
                        //Generate Header
                        MessageHeader messageHeader = MessageHeader.Generate(
                                Constants.MAGIC,
                                "headers",
                                headersPayload.getPayload().capacity(),
                                Generator.generateChecksum(headersPayload.getPayload())
                        );
                        //Generate Message {Header, Payload}
                        Message<HeadersPayload> sendMessage = new Message<>(messageHeader, headersPayload);
                        //Show generated Message
                        sendMessage.print();
                        //Sent via SocketChannel
                        if (!displayOnly)
                            this.sendMessageToPeer(sendMessage, socketChannel);
                        System.out.println("{{ END: " + (displayOnly ? "VIEWING" : "SENDING") + " HEADERS MESSAGE }}");
                    } break;

                    case "getblocks" : {
                        System.out.println("{{ " + (displayOnly ? "VIEWING" : "SENDING") + " GET_BLOCKS MESSAGE }}");
                        //Generate Payload
                        GetHeadersAndGetBlocksPayload getHeadersAndGetBlocksPayload = GetHeadersAndGetBlocksPayload.Generate(
                                Constants.VERSION,
                                new ArrayList<>(this.hashes),
                                "\0".repeat(32)
                        );
                        //Generate Header
                        MessageHeader messageHeader = MessageHeader.Generate(
                                Constants.MAGIC,
                                "getblocks",
                                getHeadersAndGetBlocksPayload.getPayload().capacity(),
                                Generator.generateChecksum(getHeadersAndGetBlocksPayload.getPayload())
                        );
                        //Generate Message {Header, Payload}
                        Message<GetHeadersAndGetBlocksPayload> sendMessage = new Message<>(messageHeader, getHeadersAndGetBlocksPayload);
                        //Show generated Message
                        sendMessage.print();
                        //Sent via SocketChannel
                        if (!displayOnly)
                            this.sendMessageToPeer(sendMessage, socketChannel);
                        System.out.println("{{ END: " + (displayOnly ? "VIEWING" : "SENDING") + " GET_BLOCKS MESSAGE }}");
                    } break;

                    case "block" : {
                        System.out.println("{{ " + (displayOnly ? "VIEWING" : "SENDING") + " BLOCK MESSAGE }}");
                        if (this.blocks.size() > 0) {
                            ArrayList<BlockPayload> asList = new ArrayList<>(this.blocks);
                            //Generate Payload
                            BlockPayload blockPayload = BlockPayload.Generate(asList.get(0).getBlockHeader(), asList.get(0).getTxList());
                            //Generate Header
                            MessageHeader messageHeader = MessageHeader.Generate(
                                    Constants.MAGIC,
                                    "block",
                                    blockPayload.getPayload().capacity(),
                                    Generator.generateChecksum(blockPayload.getPayload())
                            );
                            //Generate Message {Header, Payload}
                            Message<BlockPayload> sendMessage = new Message<>(messageHeader, blockPayload);
                            //Show generated Message
                            sendMessage.print();
                            //Sent via SocketChannel
                            if (!displayOnly)
                                this.sendMessageToPeer(sendMessage, socketChannel);
                        } else {
                            System.out.println("ERROR: NO BLOCKS.");
                        }
                        System.out.println("{{ END: " + (displayOnly ? "VIEWING" : "SENDING") + " BLOCK MESSAGE }}");
                    } break;

                    case "inv" : {
                        System.out.println("{{ " + (displayOnly ? "VIEWING" : "SENDING") + " INV MESSAGE }}");
                        //Generate Payload
                        GetDataAndInvPayload getDataAndInvPayload = GetDataAndInvPayload.Generate( this.inventoryVectors );
                        //Generate Header
                        MessageHeader messageHeader = MessageHeader.Generate(
                                Constants.MAGIC,
                                "inv",
                                getDataAndInvPayload.getPayload().capacity(),
                                Generator.generateChecksum(getDataAndInvPayload.getPayload())
                        );
                        //Generate Message {Header, Payload}
                        Message<GetDataAndInvPayload> sendMessage = new Message<>(messageHeader, getDataAndInvPayload);
                        //Show generated Message
                        sendMessage.print();
                        //Sent via SocketChannel
                        if (!displayOnly)
                            this.sendMessageToPeer(sendMessage, socketChannel);
                        System.out.println("{{ END: " + (displayOnly ? "VIEWING" : "SENDING") + " INV MESSAGE }}");
                    } break;

                    case "tx" : {
                        System.out.println("{{ " + (displayOnly ? "VIEWING" : "SENDING") + " TX MESSAGE }}");
                        ArrayList<BlockPayload> asList = new ArrayList<>(this.blocks);
                        if (this.blocks.size() > 0 && asList.get(0).getTxList().size() > 0) {
                            //Generate Payload
                            TxPayload txPayload = TxPayload.Generate(
                                    asList.get(0).getTxList().get(0).getVersionInt(),
                                    asList.get(0).getTxList().get(0).getFlagShort(),
                                    asList.get(0).getTxList().get(0).getTxInList(),
                                    asList.get(0).getTxList().get(0).getTxOutList(),
                                    asList.get(0).getTxList().get(0).getTxWitnesses(),
                                    asList.get(0).getTxList().get(0).getLocktimeInt()
                            );
                            //Generate Header
                            MessageHeader messageHeader = MessageHeader.Generate(
                                    Constants.MAGIC,
                                    "tx",
                                    txPayload.getPayload().capacity(),
                                    Generator.generateChecksum(txPayload.getPayload())
                            );
                            //Generate Message {Header, Payload}
                            Message<TxPayload> sendMessage = new Message<>(messageHeader, txPayload);
                            //Show generated Message
                            sendMessage.print();
                            //Sent via SocketChannel
                            if (!displayOnly)
                                this.sendMessageToPeer(sendMessage, socketChannel);
                        }
                        else {
                            System.out.println("ERROR: NO TRANSACTIONS.");
                        }
                        System.out.println("{{ END: " + (displayOnly ? "VIEWING" : "SENDING") + " INV MESSAGE }}");
                    } break;
                }
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    private void sendMessageToPeer(Message message, SocketChannel channel) {
        try {
            ByteBuffer byteBuffer = message.pack().flip();

            while (byteBuffer.hasRemaining())
                channel.write(byteBuffer);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Connect to random Peer from list
     */
    public void connectToPeer() {
        if (this.availablePeers.isEmpty()) {
            this.receiveIPs();
        }

        if (this.availablePeers.isEmpty()) {
            System.out.println("Unable to receive any peer IP.");
            return;
        }

        String randomIP;
        Random random = new Random();
        boolean connectionSuccessful = false;

        System.out.println("Trying to connect...");

        do {
            randomIP = this.availablePeers.get(random.nextInt(this.availablePeers.size()));

            if (!this.connectedPeers.contains(randomIP)) {
                try {
                    InetSocketAddress socketAddress = new InetSocketAddress(randomIP, 8333);
                    SocketChannel socketChannel = SocketChannel.open();
                    socketChannel.socket().connect(socketAddress, 5000);
                    socketChannel.configureBlocking(false);
                    this.selectionKeys.add(socketChannel.register(this.selector, SelectionKey.OP_READ));

                    this.connectedPeers.add(randomIP);
                    this.socketChannels.add(socketChannel);

                    this.pingNonces.put(randomIP, 0L);
                    connectionSuccessful = true;
                } catch (IOException ex) {
                    System.out.println("INFO: PEER UNREACHABLE.");
                }
            }
        } while (!connectionSuccessful);

        System.out.println("Successfully connected to " + randomIP + " on port 8333.");
    }

    /**
     *
     */
    public void receiveIPs() {
        try {
            ArrayList<String> dnses = (ArrayList<String>)Files.readAllLines(Paths.get(Constants.DNS_FILE));

            this.availablePeers.clear();

            for (String dns : dnses) {
                try {
                    //Add all addresses (IPv4 only for simplicity)
                    this.availablePeers.addAll(
                            Arrays.stream(InetAddress.getAllByName(dns))
                                    .map(InetAddress::getHostAddress)
                                    .filter(ip -> !ip.contains(":"))
                                    .collect(Collectors.toList())
                    );
                } catch (UnknownHostException ex) {
                    ex.printStackTrace();
                }
            }
        } catch (IOException ex) {
            System.out.println("ERROR reading DNS file.");
        }
    }



    public void reset() {
        for (SocketChannel sc : this.socketChannels) {
            try {
                sc.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }

        this.socketChannels.clear();
        this.connectedPeers.clear();
        this.selectionKeys.clear();
    }

    public void onExit() {
        System.out.println("Closing connections...");
        this.reset();
        this.onExit = true;
    }

    @Override
    public void run() {
        while (!this.onExit) {
            try {
                int readyChannels = this.selector.selectNow();

                if (readyChannels > 0) {
                    Set<SelectionKey> keys = selector.selectedKeys();
                    Iterator<SelectionKey> it = keys.iterator();

                    while(it.hasNext()) {
                        SelectionKey selectionKey = it.next();
                        SocketChannel channel = (SocketChannel) selectionKey.channel();

                        String socketRemoteAddress = channel.getRemoteAddress().toString();
                        socketRemoteAddress = socketRemoteAddress.substring(1, socketRemoteAddress.indexOf(":"));

                        if (selectionKey.isReadable()) {
                            System.out.println();
                            System.out.println("RECEIVED MESSAGE. Type 'read' to read.");
                            this.menu.prompt();

                            StringBuilder stringBuilder = new StringBuilder();

                            ByteBuffer headerBuffer = ByteBuffer.allocate(24);
                            try {
                                while (headerBuffer.hasRemaining())
                                    channel.read(headerBuffer);
                            } catch (IOException ex) {
                                System.out.println("INFO CONNECTION LOST WITH " + socketRemoteAddress);
                                this.menu.prompt();
                                selectionKey.cancel();
                                channel.close();
                                this.socketChannels.remove(channel);
                                continue;
                            }

                            MessageHeader messageHeader = new MessageHeader(headerBuffer.flip());
                            messageHeader.unpack();
                            stringBuilder.append(messageHeader.toString(0));

                            String command = Converter.bytesToString(messageHeader.command);

                            ByteBuffer payloadBuffer = ByteBuffer.allocate(Converter.bytesToUInt32(messageHeader.length));

                            try {
                                while (payloadBuffer.hasRemaining())
                                    channel.read(payloadBuffer);
                            } catch (IOException ex) {
                                System.out.println("INFO CONNECTION LOST WITH " + socketRemoteAddress);
                                this.menu.prompt();
                                selectionKey.cancel();
                                channel.close();
                                this.socketChannels.remove(channel);
                                continue;
                            }

                            switch (command) {
                                case "version":
                                    VersionPayload versionPayload = new VersionPayload(payloadBuffer.flip());
                                    stringBuilder.append(versionPayload.toString(0));
                                    break;
                                case "verack":
                                    VerackPayload verackPayload = new VerackPayload();
                                    stringBuilder.append(verackPayload.toString(0));
                                    break;
                                case "ping":
                                    PingPongPayload pingPayload = new PingPongPayload(payloadBuffer.flip());
                                    stringBuilder.append(pingPayload.toString(0));
                                    //Remember peer's nonce
                                    this.pingNonces.put(socketRemoteAddress, pingPayload.getNonce());
                                    break;
                                case "pong":
                                    PingPongPayload pongPayload = new PingPongPayload(payloadBuffer.flip());
                                    stringBuilder.append(pongPayload.toString(0));
                                    break;
                                case "getaddr":
                                    GetAddrPayload getAddrPayload = new GetAddrPayload();
                                    stringBuilder.append(getAddrPayload.toString(0));
                                    break;
                                case "addr":
                                    AddrPayload addrPayload = new AddrPayload(payloadBuffer.flip());
                                    stringBuilder.append(addrPayload.toString(0));
                                    //Update peers addresses
                                    this.addresses = addrPayload.getAddressesList();
                                    break;
                                case "reject":
                                    RejectPayload rejectPayload = new RejectPayload(payloadBuffer.flip());
                                    stringBuilder.append(rejectPayload.toString(0));
                                    break;
                                case "getheaders":
                                    GetHeadersAndGetBlocksPayload getHeadersAndGetBlocksPayload = new GetHeadersAndGetBlocksPayload(payloadBuffer.flip());
                                    stringBuilder.append(getHeadersAndGetBlocksPayload.toString(0));
                                    break;
                                case "headers":
                                    HeadersPayload headersPayload = new HeadersPayload(payloadBuffer.flip());
                                    stringBuilder.append(headersPayload.toString(0));
                                    break;
                                case "getdata":
                                    GetDataAndInvPayload getDataAndInvPayload = new GetDataAndInvPayload(payloadBuffer.flip());
                                    stringBuilder.append(getDataAndInvPayload.toString(0));
                                    break;
                                case "getblocks":
                                    GetHeadersAndGetBlocksPayload getHeadersAndGetBlocksPayload2 = new GetHeadersAndGetBlocksPayload(payloadBuffer.flip());
                                    stringBuilder.append(getHeadersAndGetBlocksPayload2.toString(0));
                                    break;
                                case "block":
                                    BlockPayload blockPayload = new BlockPayload(payloadBuffer.flip());
                                    stringBuilder.append(blockPayload.toString(0));
                                    //Add block
                                    this.blocks.add(blockPayload);
                                    break;
                                case "inv":
                                    GetDataAndInvPayload getDataAndInvPayload2 = new GetDataAndInvPayload(payloadBuffer.flip());
                                    stringBuilder.append(getDataAndInvPayload2.toString(0));
                                    //Add Inventory Vectors to peer set
                                    this.inventoryVectors.addAll(getDataAndInvPayload2.getInvVectList());
                                    break;
                                case "tx":
                                    TxPayload txPayload = new TxPayload(payloadBuffer.flip());
                                    stringBuilder.append(txPayload.toString(0));
                                    //Add Transactions to peer set
                                    this.transactions.add(txPayload);
                                    break;
                            }

                            this.unreadMessages.add(stringBuilder.toString());
                        }

                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException ex) {
                            ex.printStackTrace();
                        }

                        it.remove();
                    }
                }
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }

    public void printUnreadMessages() {
        System.out.println("[ RECEIVED MESSAGES ]");
        for (String s : this.unreadMessages)
            System.out.println(s);

        this.unreadMessages.clear();
        System.out.println("[ END: RECEIVED MESSAGES ]");
    }

    public void printConnectedPeers() {
        System.out.println("[ CONNECTED TO ]");
        if (this.connectedPeers.size() == 0)
            System.out.println("\tNo connected peers.");
        else
            for (String s : connectedPeers)
                System.out.println("\t" + s);
        System.out.println("[ END: CONNECTED TO ]");
    }

    public void printAvailablePeers() {
        System.out.println("[ AVAILABLE PEERS ]");
        for (String s : availablePeers)
            System.out.println("\t" + s);
        System.out.println("[ END: AVAILABLE PEERS ]");
    }
}
